<?php
/**
    Template Name: Works List
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$args = array(
    'post_type' => 'case_studies',
    'posts_per_page' => -1
);
$context['case_studies'] = Timber::get_posts($args);

$category_case_studies = get_terms('case_studies_cat', array(
    'hide_empty' => false,
));
$context['category_case_studies'] = array();
foreach ($category_case_studies as $cat) {
    $service_tmp = array();
    $service_tmp['name'] = $cat->name;
    $service_tmp['id'] =  $cat->term_id;
    array_push($context['category_case_studies'], $service_tmp);
}

if ($post->post_name == 'home') {
    Timber::render(array('home.twig'), $context);
} else {
    Timber::render(array('page-works' . $post->post_name . '.twig', 'page.twig'), $context);
}

