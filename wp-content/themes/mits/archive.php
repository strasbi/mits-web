<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array('archive.twig', 'index.twig');

$context = Timber::get_context();

$context['title'] = 'Archive';
$queried_object = get_queried_object();
if (isset($queried_object->term_id)) {
    $term_id = $queried_object->term_id;
    $context['term_id'] = $term_id;
}
$categories = get_categories();
$context['categories'] = array();
foreach ($categories as $cat) {
    $category = array();
    $category['link'] = get_category_link($cat->term_id);
    $category['name'] = $cat->name;
    array_push($context['categories'], $category);
}
if (is_day()) {
    $context['title'] = 'Archive: ' . get_the_date('D M Y');
} else if (is_month()) {
    $context['title'] = 'Archive: ' . get_the_date('M Y');
} else if (is_year()) {
    $context['title'] = 'Archive: ' . get_the_date('Y');
} else if (is_tag()) {
    $context['title'] = single_tag_title('', false);
} else if (is_category()) {
    $context['title'] = single_cat_title('', false);
    array_unshift($templates, 'archive-' . get_query_var('cat') . '.twig');
} else if (is_post_type_archive()) {
    $context['title'] = post_type_archive_title('', false);
    array_unshift($templates, 'archive-' . get_post_type() . '.twig');
}

$context['lets_talk']['acf_fc_layout'] = 'lets_talk';
$context['lets_talk']['title'] = "Let's talk";
$context['lets_talk']['short_desc'] = "We can help to make your idea comes true, just drop us a line. If you are interested join our team don’t wait. We are always looking for a talents!";

$context['posts'] = new Timber\PostQuery();

Timber::render($templates, $context);
