<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

require get_template_directory() . '/include/Benefits.php';
$benefits = new Benefits;
$context['benefits'] = $benefits->getBenefits();

$visibleAmnt = 4;
if (have_rows('sections')) {
    while (have_rows('sections')) : the_row();
        if (get_row_layout() == 'case_studies_list') {
            $visibleAmnt = get_sub_field('visible_items');
        }
    endwhile;
}

$case_study_args = array(
    'post_type' => 'case_studies',
    'posts_per_page' => $visibleAmnt
);
$case_study_query = new WP_Query($case_study_args);
$case_study_posts = $case_study_query->posts;
foreach ($case_study_posts as $index => $single_case) {
    $context['case_studies'][$index] = get_field('case_studies_header', $single_case->ID);
    $context['case_studies'][$index]['link'] = get_permalink($single_case->ID);

    $negPosArray = [-1, 1];
    $depth = rand(20, 60) / 100 * $negPosArray[rand(0, 1)];
    $frictionX = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
    $frictionY = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
    $context['case_studies'][$index]['parallaxOpts'] = [
        'depth' => $depth,
        'frictionX' => $frictionX,
        'frictionY' => $frictionY,
    ];
}

global $paged;
if (!isset($paged) || !$paged) {
    $paged = 1;
}

$posts_list_args = array(
    'post_type' => 'post',
    'posts_per_page' => 5,
    'paged' => $paged
);
//$posts_list = get_posts($posts_list_args);
$posts_list = new \Timber\PostQuery($posts_list_args);
$context['posts_list'] = $posts_list;

$categories = get_categories();
$context['categories'] = array();
foreach ($categories as $cat) {
    $category = array();
    $category['link'] = get_category_link($cat->term_id);
    $category['name'] = $cat->name;
    array_push($context['categories'], $category);
}

if ($post->post_name == 'home') {
    Timber::render(array('home.twig'), $context);
} else {
    Timber::render(array('page-' . $post->post_name . '.twig', 'page.twig'), $context);
}

//Timber::render(array('page-' . $post->post_name . '.twig', 'page.twig'), $context);
