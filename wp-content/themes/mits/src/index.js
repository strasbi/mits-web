import jQuery from 'jquery'
import polyfill_main from './scripts/polyfill'
window.jQuery = window.$ = jQuery;
import bootstrap from 'bootstrap/js/src/index'

//slick
import slick from 'slick-carousel/slick/slick'

//STYLE
import style from './styles/index.scss'

//custom scripts
import cookies from './scripts/cookies'
import dynamic_contact from './scripts/contact/dynamic_contact'
import join_us_contact from './scripts/contact/join_us_form'
import contact_form from './scripts/contact/contact_form'
import hamburger from './scripts/hamburger'
import take_around_slider from './scripts/sliders/take-around-slider'
import classic_slider from './scripts/sliders/classic-slider'
import team_slider from './scripts/sliders/team-slider'
import load_more_posts from './scripts/load_more_posts'
import typed_text from './scripts/type-text'
import radio from './scripts/radio'
import estimate from './scripts/get_estimate'
import reverse_logo from './scripts/reverse_logo'
import smoothScroll from './scripts/smoothScroll'
import show_tooltip from './scripts/_show-tooltip'
import detect_ie from './scripts/_detect_ie'
import works_list from './scripts/works_list_click'
import case_video from './scripts/case_video'

// import {paper} from './scripts/initPaper'
import {parallax} from './scripts/parallax'
// import {papersButtons} from './scripts/mitsButtons'
// import {paperClip} from './scripts/clipPaper'
import {mitsCarousels} from './scripts/sliders/brands-carousel'
import {mainCarousel} from './scripts/sliders/main-slider'

import social_links_hover from './scripts/social_links_hover'
import box_error from './scripts/box-error'
import lineAnimation from './scripts/anims/animate_lines'
import scrollMenu from './scripts/scrollMenu'
import scrollArrow from './scripts/scrollArrowPost'

window.onload = function () {
    // paper.init();
    // paperClip.init();
    parallax.init();
    // papersButtons.init();
    mitsCarousels.init();
    mainCarousel.init();
}


new WOW().init();
