import $ from 'jquery'

$(document).ready(function () {
  $(function(jq){
    // enlighterJS available ?
    if (typeof EnlighterJS == "undefined" || typeof EnlighterJS_Config == "undefined"){
      return;
    };

    // config objects
    var i =  Object.clone(EnlighterJS_Config);
    i.renderer = 'Inline';
    i.grouping = false;

    var b =  Object.clone(EnlighterJS_Config);
    b.renderer = 'Block';

    // helper to fetch all new elements
    var getNewElements = (function(s){
      // get all selected elements
      var e = document.getElements(s) || [];

      // filter new elements
      e = e.filter(function(el){
        // only visible elements are "new"
        return (el.getStyle('display') != 'none');
      });

      return e;
    });

    // re-initialize
    var _init = (function(){
      if (i.selector.inline){
        EnlighterJS.Util.Helper(getNewElements(i.selector.inline), i);
      }
      if (b.selector.block){
        EnlighterJS.Util.Helper(getNewElements(i.selector.block), b);
      }
    });

    // content update event
    jq(document).on('ajaxComplete', function(){
      _init.apply(window);
    });
  });

  let offset = 1;
  let cat = $('#mainPosts').data('category');
  let canBeLoaded = true,
    bottomOffset = 2000;

  $(window).scroll(function(){
    let data = {
      'action': 'loadmore',
      'offset' : offset,
      'cat': cat
    };
    if( $(document).scrollTop() > ( $(document).height() - bottomOffset ) && canBeLoaded == true ){
      $.ajax({
        url : dynamic_scripts.ajax,
        data:data,
        type:'POST',
        beforeSend: function( xhr ){
          canBeLoaded = false;
        },
        success:function(data){
          if( data.success ) {

            let item = $(data.data).hide();
            $('#mainPosts').append(item); // where to insert posts
            item.fadeIn(2000);
            canBeLoaded = true; // the ajax is completed, now we can run it again
            offset++;

          } else {
            canBeLoaded = false;
          }
        }
      });
    }
  });
});

