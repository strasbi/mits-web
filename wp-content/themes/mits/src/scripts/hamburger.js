import $ from 'jquery'

$(document).ready(function () {
  $(".hamburger").click(function () {
    $(this).toggleClass("is-active");
    $('.header__top').toggleClass('header__top--mobile-active');
  });
  $(".navigation__item-link").click(function (e) {
    if($(this).hasClass('dropdown-toggle')) {
      e.preventDefault();
      $('.navigation__dropdown').toggleClass('d-flex');
    } else {
      $('.hamburger').removeClass("is-active");
      $('.header__top').removeClass('header__top--mobile-active');
      $('#navbarSupportedContent').removeClass('show');
    }

  });
});

