import $ from 'jquery'

function nameValidation(name) {
    if (name.length < 3) {
        return false;
    } else {
        return true;
    }
}

function textValidation(text) {
    if (text.length < 5) {
        return false;
    } else {
        return true;
    }
}

function phoneValidation(phone) {
    let phoneRegex = /^\+{0,1}[0-9]{6,12}/;
    return phoneRegex.test(phone);
}

function emailValidation(email) {
    let mailRegex = /\S+@\S+\.\S+/;
    return mailRegex.test(email);
}

$(document).ready(function () {
    $('.estimate-send').on('click', function (e) {
        e.preventDefault();
        let data_checker = [];

        $('.main-input__field').each(function (index, el) {
            if ($(el).val() === '' && !$(el).hasClass('not-required')) {
                data_checker.push(false);
                $(el).parent('.main-input').addClass('main-input__error');
            } else {
                $(el).parent('.main-input').removeClass('main-input__error');
            }
        });

        if(!checkEmail($('.estimate-form #email'))) {
          data_checker.push(false);
        }
        if(!validatePhone($('.estimate-form #phone'))) {
          data_checker.push(false);
        }
        $('.term .term__input').each(function (index, el) {
            if ($(el).is(':checked') === false && !$(el).hasClass('not-required')) {
                data_checker.push(false);
                $(el).parent('.term').addClass('term__error');
            } else {
                $(el).parent('.term').removeClass('term__error');
            }
        });

        if (data_checker.length < 1) {

            let form = $('.estimate-form').serializeArray();
            let formdata = new FormData($('.estimate-form')[0]);
            let data = (formdata !== null) ? formdata : form.serialize();
            data.append("action", "estimate");

            let file = $('.estimate-form #file')[0].files;
            data.append('file', file[0]);

            $.ajax({
                url: dynamic_scripts.ajax,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log(response);
                    if (response.success === false) {
                        // $("html, body").animate({scrollTop: $(document).height() - $(window).height()});
                        // $('.estimate-error').removeClass('d-none');
                        // $('.estimate-success').addClass('d-none');
                        $('.box__error').addClass('box__error--visible');
                    } else {
                        $("html, body").animate({scrollTop: $(document).height() - $(window).height()});
                        $('.estimate-thanks').removeClass('d-none');
                        $('.estimate-error').addClass('d-none');
                        $('.box__error').removeClass('box__error--visible');
                    }
                },
                error: function (error) {
                    console.log(error);
                    $('.box__error').addClass('box__error--visible');
                }
            });
        }
    });

    // $('.estimate-form #name').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validateName($(this));
    // });
    $('.estimate-form #name').on('blur change', function () {
        validateName($(this));
    });

    function validateName(el) {
        if (nameValidation(el.val())) {
            el.parents('.main-input').removeClass('main-input__error')
        } else {
            el.parents('.main-input').addClass('main-input__error')
        }
    }

    // $('.estimate-form #where').on('blur keyup change', function () {
    //     if (textValidation($(this).val())) {
    //         $(this).parents('.main-input').removeClass('main-input__error')
    //     } else {
    //         $(this).parents('.main-input').addClass('main-input__error')
    //     }
    // });
    // $('.estimate-form #email').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validateEmail($(this))
    // });
    $('.estimate-form #email').on('blur change', function () {
        validateEmail($(this))
    });

    function validateEmail(el) {
        if (emailValidation(el.val())) {
            el.parents('.main-input').removeClass('main-input__error')
        } else {
            el.parents('.main-input').addClass('main-input__error')
        }
    }
      function checkEmail(el) {
        if (emailValidation(el.val())) {
          el.parents('.main-input').removeClass('main-input__error');
          return true;
        } else {
          el.parents('.main-input').addClass('main-input__error');
          return false;
        }
      }

    // $('.estimate-form #phone').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validatePhone($(this))
    // });

    $('.estimate-form #phone').on('blur change', function () {
        validatePhone($(this))
    });

    function validatePhone(el) {
        if (phoneValidation(el.val())) {
            el.parents('.main-input').removeClass('main-input__error');
            return true;
        } else {
            el.parents('.main-input').addClass('main-input__error');
            return false;
        }
    }

    // $('.estimate-form #rambo').on('change', function () {
    //     if ($(this).val()) {
    //         $(this).parents('.term').removeClass('term__error')
    //     } else {
    //         $(this).parents('.term').addClass('term__error')
    //     }
    // });
    $('.estimate-form #privacy').on('change', function () {
        if ($(this).val()) {
            $(this).parents('.term').removeClass('term__error')
        } else {
            $(this).parents('.term').addClass('term__error')
        }
    });

});
