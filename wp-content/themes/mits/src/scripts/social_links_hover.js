import $ from 'jquery'

$('document').ready(function () {

    $('.footer__social--ico, .js-social-link').on('mouseover', function () {
        let hoverColor = $(this).find('.social__svg__path').attr('data-hover-color');
        $(this).find('.social__svg__path').css('fill', hoverColor);
    });
    $('.footer__social--ico, .js-social-link').on('mouseleave', function () {
        $(this).find('.social__svg__path').removeAttr('style');
    });


});