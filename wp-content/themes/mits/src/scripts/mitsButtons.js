import paper from "paper/dist/paper-core.min"
import SimplexNoise from 'simplex-noise'

// paper.install(window)

let stuckX, stuckY
const mitsButtons = {
  canvases: {},
  noiseScale: 800,
  noiseRange: 10,
  defaultStrokeColor: null,
  defaultStrokeWidth: null,
  defaultFillColor: null,
  round: false,
  defaultScaleX: 3,
  defaultScaleY: 3,
  size: null,
  handleMouseEnter: (idx, e) => {
    mitsButtons.canvases[idx].isStuck = false;
    mitsButtons.canvases[idx].isInitiallyAnimated = true;
  },
  // reset isStuck on mouseLeave
  handleMouseLeave: (idx, e) => {
    const navItem = e.currentTarget;
    const navItemBox = navItem.getBoundingClientRect();
    // console.log(navItemBox)
    stuckX = Math.round(navItemBox.left + navItemBox.width / 2);
    stuckY = Math.round(navItemBox.top + navItemBox.height / 2);
    mitsButtons.canvases[idx].isStuck = true;
    mitsButtons.canvases[idx].isInitiallyAnimated = true;
  },
  createRect: (view, index) => {
    // we create rectangles with additional anchors in the middle
    // x---------x
    // |         |
    // |         |
    // x---------x

    let firstX = 0
    let lastX = view.bounds.width
    let firstY = 0
    let lastY = view.bounds.height
    let rect = new paper.Path.Rectangle(new paper.Rectangle(firstX, firstY, lastX, lastY))

    return rect
  },
  createPath: (view, index) => {
    // we create rectangles with additional anchors in the middle
    // x----x----x
    // |         |
    // |         |
    // x----x----x
    let path = new paper.Path()
    let centerX = view.center.x
    let firstX = centerX - centerX / 3 * 2
    let lastX = centerX + centerX / 3 * 2
    let centerY = view.center.y
    let firstY = centerY - centerY / 3 * 1.55
    let firstYCenter = centerY - centerY / 3 * 2.5
    let lastY = centerY + centerY / 3 * 2
    let lastYCenter = centerY + centerY / 3 * 2.5
    path.add(new paper.Point(firstX, firstY))
    path.add(new paper.Point(centerX, firstYCenter))
    path.add(new paper.Point(lastX, firstY))
    path.add(new paper.Point(lastX, lastY))
    path.add(new paper.Point(centerX, lastYCenter))
    path.add(new paper.Point(firstX, lastY))
    path.closed = true

    return path
  },
  createCirclePath: (view, index, width) => {
    return new paper.Path.Circle(new paper.Point(view.center.x, view.center.y), width * 1.25)
  },
  map: (value, in_min, in_max, out_min, out_max) => {
    return (
      ((value - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min
    );
  },
  noiseCoordinates: (noiseObjects, bigCoordinates, index, eventCount) => {
    // get new noise value
    // we divide event.count by noiseScale to get a very smooth value
    const noiseX = noiseObjects[index].noise2D(eventCount / mitsButtons.noiseScale, 0);
    const noiseY = noiseObjects[index].noise2D(eventCount / mitsButtons.noiseScale, 1);

    // map the noise value to our defined range
    const distortionX = mitsButtons.map(noiseX, -1, 1, -mitsButtons.noiseRange, mitsButtons.noiseRange);
    const distortionY = mitsButtons.map(noiseY, -1, 1, -mitsButtons.noiseRange, mitsButtons.noiseRange);

    // apply distortion to coordinates
    const newX = bigCoordinates[index][0] + distortionX;
    const newY = bigCoordinates[index][1] + distortionY;

    return {
      newX,
      newY
    }
  },
  initCanvas: (item, id) => {
    // fetch data attributes
    let strokeColor = (typeof item.dataset.strokeColor !== 'undefined') ? item.dataset.strokeColor : mitsButtons.defaultStrokeColor
    let strokeWidth = (typeof item.dataset.strokeWidth !== 'undefined') ? item.dataset.strokeWidth : mitsButtons.defaultStrokeWidth
    let fillColor = (typeof item.dataset.fillColor !== 'undefined') ? item.dataset.fillColor : mitsButtons.defaultFillColor
    let scaleX = (typeof item.dataset.scaleX !== 'undefined') ? item.dataset.scaleX : mitsButtons.defaultScaleX
    let scaleY = (typeof item.dataset.scaleY !== 'undefined') ? item.dataset.scaleY : mitsButtons.defaultScaleY
    let round = (typeof item.dataset.round !== 'undefined') ? item.dataset.round : mitsButtons.round
    let diameter = (typeof item.dataset.round !== 'undefined') ? item.dataset.size : mitsButtons.size

    // fetch text
    let buttonText = item.textContent.trim()
    let canvas = document.createElement("canvas")
    canvas.classList.add('js-dynamic-canvas')
    paper.setup(canvas);
    const navItemBox = item.getBoundingClientRect();

    let canvasWidth;
    let canvasHeight;
    if (round) {
      canvasWidth = diameter;
      canvasHeight = diameter;
      // canvas.style.width = `${canvasWidth}px`
      canvas.style.height = `${canvasHeight}px`
    } else {
      canvasWidth = navItemBox.width * scaleX
      canvasHeight = navItemBox.height * scaleY
      canvas.style.width = `${canvasWidth}px`
      canvas.style.height = `${canvasHeight}px`
    }

    let path
    if (round) {
      path = mitsButtons.createCirclePath(paper.view, id, diameter)
    } else {
      path = mitsButtons.createPath(paper.view, id)
    }


    const noiseObjects = path.segments.map(() => new SimplexNoise())
    let bigCoordinates = [];

    // function for linear interpolation of values
    const lerp = (a, b, n) => {
      return (1 - n) * a + n * b;
    };

    // function to map a value from one range to another range


    // path.selected = true;
    let pathBase = path.clone();
    path.strokeColor = strokeColor
    path.strokeWidth = strokeWidth
    path.fillColor = fillColor
    path.smooth()
    // let g = new paper.Group(path, rect)
    // g.clipped = true
    item.append(canvas)

    let counter = 0;
    paper.view.onFrame = function(event) {
      if (mitsButtons.canvases[id].isStuck) {
        if (bigCoordinates.length === 0) {
          path.segments.forEach((segment, i) => {
            bigCoordinates[i] = [segment.point.x, segment.point.y];
          });
        }

        // loop over all points of the polygon
        path.segments.forEach((segment, i) => {
          const noiseCoordinates = mitsButtons.noiseCoordinates(noiseObjects, bigCoordinates, i, counter)

          // set new (noisy) coodrindate of point
          segment.point.set(noiseCoordinates.newX, noiseCoordinates.newY);
          counter++
        });
      }
    }

    mitsButtons.canvases[id] = {
      item,
      canvas,
      path,
      shapeBounds: {
        width: navItemBox.width,
        height: navItemBox.height
      },
      isInitiallyAnimated: false,
      isCurrentlyAnimated: false,
      animationStartPath: {},
      isStuck: true,
    }

    item.addEventListener("mouseenter", evt => mitsButtons.handleMouseEnter(id, evt));
    item.addEventListener("mouseleave", evt => mitsButtons.handleMouseLeave(id, evt));
  },
  initButtons: () => {
    const linkItems = document.querySelectorAll(".js-button-cta");
    linkItems.forEach((item, index) => {
      mitsButtons.initCanvas(item, index)
    });
  },
  init: function () {
    this.initButtons()
  }
}

export { mitsButtons as papersButtons }
