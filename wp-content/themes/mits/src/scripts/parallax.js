import Parallax from 'parallax-js'

const mitsParallax = {
  scenes: [],
  paralaxInstances: [],
  initScene: function (scene) {
    mitsParallax.paralaxInstances.push(new Parallax(scene));
  },
  init: function() {
    this.scenes = document.querySelectorAll('.js-parallax-scene');
    this.scenes.forEach(scene => {
      this.initScene(scene)
    })
  }
}

export { mitsParallax as parallax }