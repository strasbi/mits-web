import $ from "jquery";
import Cookies from 'js-cookie'

$(document).ready(function() {
  let cookieAlert = document.querySelector('.cookiealert');
  let acceptCookies = document.querySelector('.acceptcookies');

  if (!cookieAlert) {
    return;
  }

  cookieAlert.offsetHeight;

  if (Cookies.get('acceptCookies') == null) {
    cookieAlert.classList.add('show');
  }

  acceptCookies.addEventListener('click', function () {
    Cookies.set('acceptCookies', 'yes', {expires: 365, path: '/'});
    cookieAlert.classList.remove('show');
  });
});