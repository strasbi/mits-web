import $ from 'jquery'

require('tooltipster')
require('tooltipster/dist/css/tooltipster.bundle.min.css')
// require('tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css')

$(document).ready(function () {
    var instances = $('.js-approach__tooltip--btn').tooltipster({
        animation: 'fade',
        theme: 'tooltipster-borderless',
        minWidth: 300,
        maxWidth: 331,
        trigger: 'click',
        functionInit: function (instance, helper) {
            var content = $(helper.origin).find('.tooltip_content').detach();
            instance.content(content);
        },
        functionBefore: function (instance, helper) {
            $(helper.origin).addClass('approach__tooltip--btn-circle__appear');
        },
        functionAfter: function (instance, helper) {
            $(helper.origin).removeClass('approach__tooltip--btn-circle__appear');
        }
    });

    // $('.js-approach__tooltip--btn').on('click', function () {
    //     if ($(this).hasClass('approach__tooltip--btn-circle__appear')) {
    //         $(this).tooltipster('close');
    //         $(this).removeClass('approach__tooltip--btn-circle__appear');
    //     } else {
    //         $(this).tooltipster('open');
    //         $(this).addClass('approach__tooltip--btn-circle__appear');
    //     }
    // });
    // $(document).on('blur', '.js-approach__tooltip--btn', function () {
    //     console.log($(this));
    //     $(this).removeClass('approach__tooltip--btn-circle__appear');
    //     $.each(instances, function (i, instance) {
    //         console.log($(this));
    //         instance.close();
    //     });
    // });
    // var inst = el.tooltipster('instance')
    // inst.open()

    // $("html").on('mouseover', '.js-approach__tooltip--btn', function () {
    //     $(this).parent().siblings('.approach-box__tooltip').addClass('approach-box__tooltip--active');
    // })
    // $("html").on('mouseleave', '.js-approach__tooltip--btn', function () {
    //     $(this).parent().siblings('.approach-box__tooltip').removeClass('approach-box__tooltip--active');
    // })
});