import $ from "jquery";

$(document).ready(function () {
    $('.box__error').on('click', '.box__close', function () {
        $(this).parents('.box__error').removeClass('box__error--visible');
    });
});