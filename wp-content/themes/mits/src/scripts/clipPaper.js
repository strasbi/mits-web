import paper from "paper/dist/paper-core.min"
import SimplexNoise from 'simplex-noise'

paper.install(window)

const mitsClip = {
    initScale: 0.7, // smaller to be able to fit in bounds
    noiseScale: 500,
    noiseRange: 10,
    slowFactor: 1,
    xAxisInverted: 0,
    yAxisInverted: 0,
    defaultFillColor: '#EEF2F4',
    canvases: [],
    map: (value, in_min, in_max, out_min, out_max) => {
        return (
          ((value - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min
        );
    },
    noiseCoordinates: (noiseObjects, bigCoordinates, index, eventCount) => {
        // get new noise value
        // we divide event.count by noiseScale to get a very smooth value
        const noiseX = noiseObjects[index].noise2D(eventCount / mitsClip.noiseScale, 0);
        const noiseY = noiseObjects[index].noise2D(eventCount / mitsClip.noiseScale, 1);

        // map the noise value to our defined range
        const distortionX = mitsClip.map(noiseX, -1, 1, -mitsClip.noiseRange, mitsClip.noiseRange);
        const distortionY = mitsClip.map(noiseY, -1, 1, -mitsClip.noiseRange, mitsClip.noiseRange);

        // apply distortion to coordinates
        const newX = bigCoordinates[index][0] + distortionX;
        const newY = bigCoordinates[index][1] + distortionY;

        return {
            newX,
            newY
        }
    },
    initCanvas: function (canvas) {
        let svgPath = canvas.dataset.svg
        let svgPathBg = canvas.dataset.bgSvg
        let scale = (canvas.dataset.scale) ? parseFloat(canvas.dataset.scale) : 1
        mitsClip.noiseScale = (canvas.dataset.noiseScale) ? parseFloat(canvas.dataset.noiseScale) : mitsClip.noiseScale
        mitsClip.noiseRange = (canvas.dataset.noiseRange) ? parseFloat(canvas.dataset.noiseRange) : mitsClip.noiseRange
        let slowFactor = (canvas.dataset.slowFactor) ? parseFloat(canvas.dataset.slowFactor) : this.slowFactor
        let xAxisInverted = (canvas.dataset.xAxisInverted) ? !!parseInt(canvas.dataset.xAxisInverted) : !!this.xAxisInverted
        let yAxisInverted = (canvas.dataset.yAxisInverted) ? !!parseInt(canvas.dataset.yAxisInverted) : !!this.yAxisInverted
        let fillColor = (typeof canvas.dataset.fillColor !== 'undefined') ? canvas.dataset.fillColor : mitsClip.defaultFillColor
        let imgEl = canvas.querySelector('img')

        paper.setup(canvas);

        var raster = new paper.Raster(imgEl)
        raster.position = paper.view.center
        // paper.view.bounds.height = raster.height
        console.log(canvas.width, paper.view.bounds.width, paper.view.bounds.height)
        raster.width = paper.view.bounds.width
        raster.height = paper.view.bounds.height

        let center, width, height;
        let noiseObjectsBg
        let bigCoordinatesBg;
        let pathBg;

        if (typeof svgPathBg !== 'undefined') {
            pathBg = new paper.Path(svgPathBg).scale(this.initScale * scale);
            pathBg.bounds.width = paper.view.bounds.width * 0.85
            pathBg.bounds.x = pathBg.bounds.x / 4
            pathBg.bounds.height = paper.view.bounds.height * 0.85
            pathBg.bounds.y = pathBg.bounds.y / 4
            pathBg.fillColor = fillColor;

            noiseObjectsBg = pathBg.segments.map(() => new SimplexNoise())
            bigCoordinatesBg = [];
        }

        let path = new paper.Path(svgPath).scale(this.initScale * scale);

        // path should be resized
        path.bounds.width = paper.view.bounds.width * 0.9
        path.bounds.x = path.bounds.x / 4
        path.bounds.height = paper.view.bounds.height * 0.9
        path.bounds.y = path.bounds.y / 2

        let g = new paper.Group(path, raster)
        g.clipped = true

        const noiseObjects = path.segments.map(() => new SimplexNoise())
        let bigCoordinates = [];

        let counter = 0;
        paper.view.onFrame = function (event) {
            if (bigCoordinates.length === 0) {
                path.segments.forEach((segment, i) => {
                    bigCoordinates[i] = [segment.point.x, segment.point.y];
                });
            }

            // loop over all points of the polygon
            path.segments.forEach((segment, i) => {
                const noiseCoordinates = mitsClip.noiseCoordinates(noiseObjects, bigCoordinates, i, counter)

                // set new (noisy) coodrindate of point
                segment.point.set(noiseCoordinates.newX, noiseCoordinates.newY);
                counter++
            });

            if (typeof svgPathBg !== 'undefined') {
                if (bigCoordinatesBg.length === 0) {
                    pathBg.segments.forEach((segment, i) => {
                        bigCoordinatesBg[i] = [segment.point.x, segment.point.y];
                    });
                }

                // loop over all points of the polygon
                pathBg.segments.forEach((segment, i) => {
                    const noiseCoordinatesBg = mitsClip.noiseCoordinates(noiseObjectsBg, bigCoordinatesBg, i, counter)

                    // set new (noisy) coodrindate of point
                    segment.point.set(noiseCoordinatesBg.newX, noiseCoordinatesBg.newY);
                    counter++
                });
            }
        }

        paper.view.onResize = (view) => {
            // initPath(view)
            center = paper.view.center;
            // width = paper.view.bounds.width / 2;
            // height = paper.view.bounds.height / 2;
            width = view.size.width / 2;
            height = view.size.width / 2;

            let deltaX = view.delta.width
            let deltaY = view.delta.height

            // let pathB = path.clone()
            // path.segments = [];
            // path.add(paper.view.bounds.bottomLeft);
            // for (var i = 0; i < points; i++) {
            //     var point = new paper.Point(pathB.segments[i].point.x + (deltaX / 2), pathB.segments[i].point.y + (deltaY / 2));
            //     path.add(point);
            // }
            // path.size.width += (deltaX / 2)
            // path.size.height += (deltaY / 2)
            // path.add(paper.view.bounds.bottomRight);
            // console.log('pre', path.segments[0].point.x, path.segments[0].point.y)
            // for (var i = 0; i < points; i++) {
            //     path.segments[i].point.x = pathB.segments[i].point.x + (deltaX / 2)
            //     path.segments[i].point.y = pathB.segments[i].point.y + (deltaY / 2)
            // }
            // console.log('post', path.segments[0].point.x, path.segments[0].point.y)
            path.scale(1, 0.5)
            // path.bounds.width = view.size.width * ratio
            // path.bounds.height = view.size.height * 0.5
            //
            // path.bounds.x = path.bounds.x / 4
            // path.bounds.y = path.bounds.y / 2
        }
    },
    initPaperInstances: function () {
        mitsClip.canvases.forEach(canvas => {
            this.initCanvas(canvas);
        });
    },
    init: function () {
        this.canvases = document.querySelectorAll('.js-svgCanvas-mask');
        this.initPaperInstances();
    }
}

export {mitsClip as paperClip}
