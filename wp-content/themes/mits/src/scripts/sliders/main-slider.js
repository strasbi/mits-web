import $ from 'jquery'
import imagesLoaded from "imagesloaded";

const mainSlickCarousel = {
    isLowerThanBreakPoint: function (breakpointValue) {
        return window.matchMedia("(max-width: " + breakpointValue + "px)").matches;
    },
    initSlick: function () {
        let self = this;
        let breakpointValue = 991;
        let isBelow = self.isLowerThanBreakPoint(breakpointValue);
        let slickOpts = {
            dots: false,
            arrows: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        centerMode: false,
                        adaptiveHeight: true,
                        autoplay: true,
                        autoplaySpeed: 2000
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                        dots: false,
                        infinite: true,
                        arrows: false,
                        autoplay: true,
                        autoplaySpeed: 2000
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                        dots: false,
                        arrows: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000
                    }
                }
            ]
        };
        var slickObjsContent = {};
        var slickObjs = $('.main-slider__slider');
        var i = 1;
        slickObjs.each(function () {
            slickObjsContent[i++] = $(this).html();
            var slickOb = $(this);
            slickOb.slick(slickOpts);
            if (!isBelow) {
                slickOb.slick('unslick');
                mainSlickCarousel.initMasonry();
            }
        });
        $(window).on('resize', function (e) {
            e.preventDefault();
            if (self.isLowerThanBreakPoint(breakpointValue)) {
                // window width is less than 480
                slickObjs.each(function () {
                    var slickOb = $(this);
                    slickOb.slick('init');
                });
            } else {
                // window width is more than 900px
                var i = 1;
                slickObjs.each(function () {
                    var slickOb = $(this);
                    slickOb.slick('unslick');
                    slickOb.html(slickObjsContent[i++]);
                });
                mainSlickCarousel.initMasonry();
            }
        });
    },
    resizeMasonryItem: function(item){
        /* Get the grid object, its row-gap, and the size of its implicit rows */
        var grid = document.getElementsByClassName('masonry')[0];
        if( grid ) {
            var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap')),
              rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows')),
              gridImagesAsContent = item.querySelector('.masonry-content');

            /*
             * Spanning for any brick = S
             * Grid's row-gap = G
             * Size of grid's implicitly create row-track = R
             * Height of item content = H
             * Net height of the item = H1 = H + G
             * Net height of the implicit row-track = T = G + R
             * S = H1 / T
             */
            var rowSpan = Math.ceil((item.querySelector('.masonry-content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));

            /* Set the spanning as calculated above (S) */
            item.style.gridRowEnd = 'span '+rowSpan;
            if(gridImagesAsContent) {
                // console.log(item.querySelector('.masonry-content'));
                item.querySelector('.masonry-content').style.height = item.getBoundingClientRect().height + "px";
            }
        }
    },

    /**
     * Apply spanning to all the masonry items
     *
     * Loop through all the items and apply the spanning to them using
     * `resizeMasonryItem()` function.
     *
     * @uses resizeMasonryItem
     * @link https://w3bits.com/css-grid-masonry/
     */
    resizeAllMasonryItems: function(){
        // Get all item class objects in one list
        var allItems = document.querySelectorAll('.masonry-item');

        /*
         * Loop through the above list and execute the spanning function to
         * each list-item (i.e. each masonry item)
         */
        if( allItems ) {
            // console.log('beforeResizeAll', allItems);
            for(var i=0;i<allItems.length;i++){
                mainSlickCarousel.resizeMasonryItem(allItems[i]);
            }
        }
    },

    /**
     * Resize the items when all the images inside the masonry grid
     * finish loading. This will ensure that all the content inside our
     * masonry items is visible.
     *
     * @uses ImagesLoaded
     * @uses resizeMasonryItem
     * @link https://w3bits.com/css-grid-masonry/
     */
    waitForImages: function () {
        //var grid = document.getElementById("masonry");
        var allItems = document.querySelectorAll('.masonry-item');
        if( allItems ) {
            for(var i=0;i<allItems.length;i++){
                imagesLoaded( allItems[i], function(instance) {
                    var item = instance.elements[0];
                    mainSlickCarousel.resizeMasonryItem(item);
                } );
            }
        }
    },
    initMasonry: function() {

        /* Resize all the grid items on the load and resize events */
        var masonryEvents = ['load', 'resize'];
        masonryEvents.forEach( function(event) {
            window.addEventListener(event, mainSlickCarousel.resizeAllMasonryItems);
        } );

        /* Do a resize once more when all the images finish loading */
        mainSlickCarousel.waitForImages();
    },
    init: function () {
        this.initSlick()
    }
};


export {mainSlickCarousel as mainCarousel}
