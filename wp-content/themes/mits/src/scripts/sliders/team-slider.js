import $ from 'jquery'

$(document).ready(function () {
    let team_slider_settings = {
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        adaptiveHeight: true,
        prevArrow: $('.team-slider__prev'),
        nextArrow: $('.team-slider__next'),
        autoplay: false,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    adaptiveHeight: true,
                    autoplaySpeed: 2000
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    adaptiveHeight: true,
                    autoplaySpeed: 2000,
                }
            }
        ]
    };
    $('.team-slider__slides').slick(team_slider_settings);
});
