import $ from 'jquery'




$(document).ready(function () {

  let titleImages = document.getElementsByClassName('slide__item__title');
  for (var i = 0; i < titleImages.length; i++) {
    let positionInfo = titleImages.item(i).getBoundingClientRect();
    let width;
    width = positionInfo.width - 20;
    titleImages.item(i).style.left = -width + 'px';
  }

  let slick_settings = {
    dots: false,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 30000,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    pauseOnHover: false,
    rtl: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          variableWidth: true,
          speed: 40000,
        }
      },
    ]
  };
  $('.take-around-slider').slick(slick_settings);

});
