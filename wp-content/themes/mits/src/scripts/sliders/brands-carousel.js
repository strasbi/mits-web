import $ from 'jquery'

const mitsSlickCarousels = {
    isLowerThanBreakPoint: function (breakpointValue) {
        return window.matchMedia("(max-width: " + breakpointValue + "px)").matches;
    },
    initSlick: function () {
        let self = this;
        let breakpointValue = 991;
        let isBelow = self.isLowerThanBreakPoint(breakpointValue);
        let slickOpts = {
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            centerMode: true,
            responsive: [
                {
                    breakpoint: breakpointValue,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000
                    }
                }
            ]
        };
        var slickObjsContent = {};
        var slickObjs = $('.box__slider');
        var i = 1;
        slickObjs.each(function () {
            slickObjsContent[i++] = $(this).html();
            var slickOb = $(this);
            slickOb.slick(slickOpts);
            if (!isBelow) {
                slickOb.slick('unslick');
            }
        });
        $(window).on('resize', function (e) {
            e.preventDefault();
            if (self.isLowerThanBreakPoint(breakpointValue)) {
                // window width is less than 480
                slickObjs.each(function () {
                    var slickOb = $(this);
                    slickOb.slick('init');
                });
            } else {
                // window width is more than 900px
                var i = 1;
                slickObjs.each(function () {
                    var slickOb = $(this);
                    slickOb.slick('unslick');
                    slickOb.html(slickObjsContent[i++]);
                });
            }
        });
    },

    init: function () {
        this.initSlick()
    }
};


export {mitsSlickCarousels as mitsCarousels}