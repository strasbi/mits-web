import $ from 'jquery'

$(document).ready(function () {
    $("a").on('click', function (event) {

        if (this.hash !== "") {

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top - 100
            }, 400, function () {

                window.location.hash = hash;
            });
            return false;
        } // End if
    });
});
