import $ from 'jquery'

$(document).ready(function () {
  function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      document.getElementsByTagName('video')[0].play();
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      document.getElementsByTagName('video')[0].play();
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      document.getElementsByTagName('video')[0].play();
    }
    // other browser
    return false;
  }
  detectIE();
});