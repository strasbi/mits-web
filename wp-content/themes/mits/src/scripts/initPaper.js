import paper from "paper/dist/paper-core.min"

/* forEach IE function fixed */
(function () {
  if ( typeof NodeList.prototype.forEach === "function" ) return false;
  NodeList.prototype.forEach = Array.prototype.forEach;
})();

paper.install(window)

const mits = {
    initScale: 0.7, // smaller to be able to fit in bounds
    slowFactor: 1,
    xAxisInverted: 0,
    yAxisInverted: 0,
    defaultFillColor: '#EEF2F4',
    canvases: [],
    initCanvas: function (canvas) {
        let svgPath = canvas.dataset.svg
        let scale = (canvas.dataset.scale) ? parseFloat(canvas.dataset.scale) : 1
        let slowFactor = (canvas.dataset.slowFactor) ? parseFloat(canvas.dataset.slowFactor) : this.slowFactor
        let xAxisInverted = (canvas.dataset.xAxisInverted) ? !!parseInt(canvas.dataset.xAxisInverted) : !!this.xAxisInverted
        let yAxisInverted = (canvas.dataset.yAxisInverted) ? !!parseInt(canvas.dataset.yAxisInverted) : !!this.yAxisInverted
        let fillColor = (typeof canvas.dataset.fillColor !== 'undefined') ? canvas.dataset.fillColor : mits.defaultFillColor
        paper.setup(canvas);

        let center, width, height;
        let smooth = true;
        let path = new paper.Path(svgPath).scale(this.initScale * scale);

        path.bounds.width = paper.view.bounds.width * 0.8
        // path.bounds.x = path.bounds.x / 4

        path.bounds.height = paper.view.bounds.height * 0.8
        // path.bounds.y = path.bounds.y / 2
        path.position = paper.view.center
        let pathBase = path.clone();
        path.fillColor = fillColor;
        let points = path.segments.length;

        paper.view.onFrame = function (event) {
            // pathHeight += (center.y - mousePos.y - pathHeight) / 10;
            for (var i = 0; i < points; i++) {
                let sinSeed = event.count + (i + i % 10) * 10;
                // var sinHeight = Math.sin(sinSeed / 2000) * pathHeight;
                let speedY = 120 * slowFactor
                let speedX = 100 * slowFactor
                let xAxisInvert = (xAxisInverted) ? -1 : 1;
                let yAxisInvert = (yAxisInverted) ? -1 : 1;
                let yPos = Math.sin(sinSeed / speedY) * 30 * xAxisInvert;
                let xPos = Math.cos(sinSeed / speedX) * 50 * yAxisInvert;

                path.segments[i].point.y = pathBase.segments[i].point.y + yPos;
                path.segments[i].point.x = pathBase.segments[i].point.x + xPos;
            }
            if (smooth)
                path.smooth({type: 'continuous'});
        }

        paper.view.onResize = (view) => {
            console.log('resize', view, paper.view.bounds)
            // initPath(view)
            center = paper.view.center;
            // width = paper.view.bounds.width / 2;
            // height = paper.view.bounds.height / 2;
            width = view.size.width / 2;
            height = view.size.width / 2;

            let deltaX = view.delta.width
            let deltaY = view.delta.height

            // let pathB = path.clone()
            // path.segments = [];
            // path.add(paper.view.bounds.bottomLeft);
            // for (var i = 0; i < points; i++) {
            //     var point = new paper.Point(pathB.segments[i].point.x + (deltaX / 2), pathB.segments[i].point.y + (deltaY / 2));
            //     path.add(point);
            // }
            // path.size.width += (deltaX / 2)
            // path.size.height += (deltaY / 2)
            // path.add(paper.view.bounds.bottomRight);
            // console.log('pre', path.segments[0].point.x, path.segments[0].point.y)
            // for (var i = 0; i < points; i++) {
            //     path.segments[i].point.x = pathB.segments[i].point.x + (deltaX / 2)
            //     path.segments[i].point.y = pathB.segments[i].point.y + (deltaY / 2)
            // }
            // console.log('post', path.segments[0].point.x, path.segments[0].point.y)
            path.scale(1, 0.5)
            console.log(deltaX, deltaY, path)
            // path.bounds.width = view.size.width * ratio
            // path.bounds.height = view.size.height * 0.5
            //
            // path.bounds.x = path.bounds.x / 4
            // path.bounds.y = path.bounds.y / 2
        }
    },
    initPaperInstances: function () {
        mits.canvases.forEach(canvas => {
            this.initCanvas(canvas);
        });
    },
    init: function () {
        this.canvases = document.querySelectorAll('.js-svgCanvas');
        this.initPaperInstances();
    }
}

export {mits as paper}
// window.onload = function() {
//   mits.init();
// }