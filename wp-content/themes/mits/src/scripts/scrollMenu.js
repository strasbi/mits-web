let prevScrollpos = window.pageYOffset;
window.onscroll = function () {
  let currentScrollPos = window.pageYOffset;
  if (currentScrollPos > 200) {
    if (prevScrollpos > currentScrollPos) {
      document.getElementById("navbar").classList.add('navbar--scroll');
      document.getElementById("navbar").style.top = "0px";
      var el = document.querySelector('.navbar-case-study .animated_logo')
      if (el) {
        el.classList.remove('animated_logo--black')
      }
      var lines = document.querySelectorAll('.navbar-case-study .line--black')
      if (lines) {
        for (let i = 0; i<lines.length; i++) {
          lines[i].classList.remove('line--black');
        }
      }
    } else {
      document.getElementById("navbar").classList.add('navbar--scroll');
      document.getElementById("navbar").style.top = "-110px";
    }
  } else {
    document.getElementById("navbar").style.top = "-110px";
    if (currentScrollPos < 100) {
      document.getElementById("navbar").style.top = "0px";
      document.getElementById("navbar").classList.remove('navbar--scroll');
      var el = document.querySelector('.navbar-case-study .animated_logo')
      if (el) {
        el.classList.add('animated_logo--black')
      }
      var lines = document.querySelectorAll('.navbar-case-study .line')
      if (lines) {
        for (let i = 0; i<lines.length; i++) {
          if (i == 0 || i == 2) {
            lines[i].classList.add('line--black');
          }
        }
      }
    }
  }
  prevScrollpos = currentScrollPos;
  if (currentScrollPos > 550 && window.innerHeight + window.scrollY < document.body.offsetHeight) {
    if (document.getElementById('servicesMenu')) {
      document.getElementById("servicesMenu").style.bottom = "0px";
    }
  } else  {
    if (document.getElementById('servicesMenu')) {
      document.getElementById("servicesMenu").style.bottom = "-70px";
    }
  }


}


