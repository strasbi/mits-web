import $ from 'jquery'

$(document).ready(function () {

  let offsetMore = 6;

  $('.js-click-work-category').click(function () {

    let offset = 0;
    offsetMore = 6;

    $(this).toggleClass('checked');

    let categories = [];
    $(".js-click-work-category").each(function () {
      if ($(this).hasClass('checked')) {
        categories.push($(this).data('id'));
      }
    });

    let data = {
      'action': 'loadmore_works',
      'offset': offset,
      'cat': categories
    };
    $.ajax({
      url: dynamic_scripts.ajax,
      data: data,
      type: 'POST',
      beforeSend: function (xhr) {

      },
      success: function (data) {
        if (data.success) {
          let response = data.data.html;
          $('#worksList').html(response);
          if (data.data.count < 6) {
            $("#moreWorksContainer").removeClass('d-flex');
            $("#moreWorksContainer").addClass('d-none');
          } else {
            $("#moreWorksContainer").removeClass('d-none');
            $("#moreWorksContainer").addClass('d-flex');
          }
        } else {
          $('#worksList').html("");
          $("#moreWorksContainer").removeClass('d-flex');
          $("#moreWorksContainer").addClass('d-none');
        }
      }
    });


  });

  $('#moreWorks').click(function () {

    let categories = [];
    $(".js-click-work-category").each(function () {
      if ($(this).hasClass('checked')) {
        categories.push($(this).data('id'));
      }
    });

    let data = {
      'action': 'loadmore_works',
      'offset': offsetMore,
      'cat': categories
    };
    $.ajax({
      url: dynamic_scripts.ajax,
      data: data,
      type: 'POST',
      beforeSend: function (xhr) {

      },
      success: function (data) {
        if (data.success) {
          let response = data.data.html;
          $('#worksList').append(response);
          if (data.data.count < 6) {
            $("#moreWorksContainer").removeClass('d-flex');
            $("#moreWorksContainer").addClass('d-none');
          } else {
            $("#moreWorksContainer").removeClass('d-none');
            $("#moreWorksContainer").addClass('d-flex');
            offsetMore = offsetMore + 6;
          }
        } else {
          $("#moreWorksContainer").removeClass('d-flex');
          $("#moreWorksContainer").addClass('d-none');
        }
      }
    });


  });


});
