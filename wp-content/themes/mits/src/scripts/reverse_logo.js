import $ from 'jquery'

$(document).ready(function () {
    let logo = $('.logo__reverser');
    let logo_dark = logo.attr('src');
    let logo_white = logo.attr('data-logo-mobile-src');

    if (($(window).width() < 991) && $('#header-top').hasClass('header__top--mobile-active')) {
        $(logo).attr('src', logo_white);
    } else {
        $(logo).attr('src', logo_dark);
    }
    $(window).resize(function () {
        if (($(window).width() < 991) && $('#header-top').hasClass('header__top--mobile-active')) {
            $(logo).attr('src', logo_white);
        } else {
            $(logo).attr('src', logo_dark);
        }
    });
});