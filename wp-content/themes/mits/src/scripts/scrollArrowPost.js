document.addEventListener("DOMContentLoaded", function() {
  if (document.body.classList.contains('single-post')) {
    window.onscroll = function () {
      let currentScrollPos = window.pageYOffset;
      var footerOffset = document.getElementById("footerContainer").offsetTop;
      if (currentScrollPos > 700 && (window.innerHeight + currentScrollPos) < footerOffset) {
        document.querySelector(".post__arrow--left").classList.add('active');
        document.querySelector(".post__arrow--right").classList.add('active');
      } else {
        document.querySelector(".post__arrow--left").classList.remove('active');
        document.querySelector(".post__arrow--right").classList.remove('active');
      }
    }
  }
});