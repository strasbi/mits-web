import $ from 'jquery'

$(document).ready(function () {
    $('.radio-button__input').on('change', function () {
        if ($(this).parent('.radio-button').hasClass('radio-button__checked')) {
           // $(this).parent('.radio-button').toggleClass('radio-button__checked');
            $(this).parent('.radio-button').removeClass('radio-button__checked');
        } else {
           // $('.radio-button').removeClass('radio-button__checked');
            $(this).parent('.radio-button').addClass('radio-button__checked');
        }
    });

    $('.term .term__input').on('change', function () {
        $(this).parent('.term').toggleClass('term--checked');
    });
    $('.term .term__checkmark').on('change', function () {
        $(this).parent('.term').toggleClass('term--checked');
    });

});
