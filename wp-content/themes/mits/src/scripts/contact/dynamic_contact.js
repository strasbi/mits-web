import $ from 'jquery'

function nameValidation(name) {
    if (name.length < 3) {
        return false;
    } else {
        return true;
    }
}

function textValidation(text) {
    if (text.length < 10) {
        return false;
    } else {
        return true;
    }
}

function phoneValidation(phone) {
    let phoneRegex = /^\+{0,1}[1-9]{6,12}/;
    return phoneRegex.test(phone);
}

function emailValidation(email) {
    let mailRegex = /\S+@\S+\.\S+/;
    return mailRegex.test(email);
}

function submit_form_contact() {
    var form = $("#contact_form").serializeArray();
    var formdata = new FormData(document.getElementById("contact_form"));
    var data = (formdata !== null) ? formdata : form.serialize();
    data.append("action", "contact_form");

    let title = $('#contact_form').siblings('.form__title');
    let old_messages = $(title).attr("data-type");
    let error_message = "Ups, something wrong!";
    let success_message = "Thx for message!";

    $.ajax({
        url: dynamic_scripts.ajax,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            console.log(response);
            $('#contact_form .input__group').find('.input__element, .text__element').removeClass('error');
            if (response.success === false) {
                Object.keys(response.data.error).forEach(function (key) {
                    $(`#${key}`).addClass('error');
                    console.log(key);
                });
                $('.box__error').addClass('box__error--visible');
            } else {
                $('#contact_form').slideUp("fast");
                $(title).html(success_message);
                $('.box__error').removeClass('box__error--visible');
            }
        },
        error: function (error) {
            $('.box__error').addClass('box__error--visible');
        }
    });
}

$('document').ready(function () {
    $('#contact_form_btn').on('click', function (event) {
        event.preventDefault();
        submit_form_contact();
    });

    // $('#contact_form #name').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validateName($(this));
    // });

    $('#contact_form #name').on('blur change', function () {
        validateName($(this));
    });

    function validateName(el) {
        if (nameValidation(el.val())) {
            el.removeClass('error')
        } else {
            el.addClass('error')
        }
    }

    // $('#contact_form #message').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validateMessage($(this))
    // });

    $('#contact_form #message').on('blur change', function () {
        validateMessage($(this))
    });

    function validateMessage(el) {
        if (textValidation(el.val())) {
            el.removeClass('error')
        } else {
            el.addClass('error')
        }
    }

    // $('#contact_form #email_or_number').on('keyup', function (e) {
    //     if (e.which === 9) return;
    //     validateEmailPhone($(this))
    // })

    $('#contact_form #email_or_number').on('blur change', function () {
        validateEmailPhone($(this))
    })

    function validateEmailPhone(el) {
        if (emailValidation(el.val()) || phoneValidation(el.val())) {
            el.removeClass('error')
        } else {
            el.addClass('error')
        }
    }

});
