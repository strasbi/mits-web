import $ from 'jquery'

function nameValidation(name) {
  if (name.length < 3) {
    return false;
  } else {
    return true;
  }
}

function textValidation(text) {
  if (text.length < 10) {
    return false;
  } else {
    return true;
  }
}

function phoneValidation(phone) {
  let phoneRegex = /^\+{0,1}[0-9]{6,12}/;
  return phoneRegex.test(phone);
}

function emailValidation(email) {
  let mailRegex = /\S+@\S+\.\S+/;
  return mailRegex.test(email);
}

function submit_form_join_us() {

  let form = $("#join_us_form").serializeArray();
  let formdata = new FormData(document.getElementById("join_us_form"));
  let data = (formdata !== null) ? formdata : form.serialize();
  data.append("action", "join_us_contact_form");

  let error_message = "Ups, something wrong!";
  let success_message = "Thx for message!";

  let flag = false;
  $(".checkbox-input.required").each(function () {
    if (!$(this).prop('checked')) {
      $(this).parent().addClass("error");
      flag = true;
    } else {
      $(this).parent().removeClass("error");
    }
  });

  if (flag) {

    return;
  }
  if ($('#cv').val()) {
    var fileSize = ($('#cv')[0].files[0].size) / 1024 / 1024;
    if (fileSize > 8) {
      $(`[data-id="cv"]`).addClass('error');
      $(`[data-id="cv"]`).html("Max file size is 8MB");
      $("#label_cv").addClass("error");
      return;
    } else {
      $("#label_cv").removeClass("error");
      $(`[data-id="cv"]`).addClass('error');
      $(`[data-id="cv"]`).html("");
    }
  }
  $.ajax({
    url: dynamic_scripts.ajax,
    type: 'POST',
    dataType: 'JSON',
    data: data,
    contentType: false,
    processData: false,
    success: function (response) {
      $('#join_us_form .input__group').find('.error', '.input__status').removeClass('error');
      if (response.success === false) {
        Object.keys(response.data).forEach(function (key) {
          let item = response.data;
          $(`#${key}`).addClass('error');
          $(`[data-id="${key}"]`).addClass('error');
          $(`[data-id="${key}"]`).html(item[key]);
        });
        $('.box__error').addClass('box__error--visible');

      } else {
        $(".js-popup-join-us").removeClass("invisible");
        $(".js-popup-join-us").removeClass("d-none");
        $('.estimate-error').addClass('d-none');
        $('.box__error').removeClass('box__error--visible');


      }
    },
    error: function (error) {
      $('.box__error').addClass('box__error--visible');
    }
  });
}


$('document').ready(function () {

  var inputs = document.querySelectorAll('.inputfile');
  Array.prototype.forEach.call(inputs, function (input) {
    let label = input.nextElementSibling;
    let labelVal = label.innerHTML;

    input.addEventListener('change', function (e) {
      let fileName = e.target.value.split("\\").pop();
      if (fileName)
        label.querySelector('span').innerHTML = fileName;
      else
        label.innerHTML = labelVal;
    });

  });

  $('#join_us_form').on('submit', function (event) {
    event.preventDefault();
    submit_form_join_us();
  });

  $('.js-close-popup-join-us').on('click', function (event) {
    $(".js-popup-join-us").addClass("d-none");
    $(".js-popup-join-us").addClass("invisible");
  });


  // $('#join_us_form #name').on('keyup', function (e) {
  //   if (e.which === 9) return;
  //   validateName($(this))
  // });

  $('#join_us_form #name').on('blur change', function () {
    validateName($(this))
  });

  function validateName(el) {
    if (nameValidation(el.val())) {
      el.removeClass('error');
      $(`[data-id="name"]`).removeClass('error');
    } else {
      el.addClass('error');
      $(`[data-id="name"]`).addClass('error');
    }
  }

  // $('#join_us_form #message').on('keyup', function (e) {
  //   if (e.which === 9) return;
  //   validateMessage($(this))
  // });

  $('#join_us_form #message').on('blur change', function () {
    validateMessage($(this))
  });

  function validateMessage(el) {
    if (textValidation(el.val())) {
      el.removeClass('error');
      $(`[data-id="message"]`).removeClass('error');
    } else {
      el.addClass('error');
      $(`[data-id="message"]`).addClass('error');
    }
  }

  // $('#join_us_form #email').on('keyup', function (e) {
  //   if (e.which === 9) return;
  //   validateEmail($(this))
  // });

  $('#join_us_form #email').on('blur change', function () {
    validateEmail($(this))
  });

  function validateEmail(el) {
    if (emailValidation(el.val())) {
      el.removeClass('error');
      $(`[data-id="email"]`).removeClass('error');
    } else {
      el.addClass('error');
      $(`[data-id="email"]`).addClass('error');
    }
  }

  // $('#join_us_form #phone').on('keyup', function (e) {
  //   if (e.which === 9) return;
  //   validatePhone($(this))
  // });

  $('#join_us_form #phone').on('blur change', function () {
    validatePhone($(this))
  });

  function validatePhone(el) {
    if (phoneValidation(el.val())) {
      el.removeClass('error');
      $(`[data-id="phone"]`).removeClass('error');
    } else {
      el.addClass('error');
      $(`[data-id="phone"]`).addClass('error');
    }
  }

  $('#join_us_form #rules1').on('change', function () {
    if ($(this).val()) {
      $(this).parent().removeClass('error')
    } else {
      $(this).parent().addClass('error')
    }
  });
  $('#join_us_form #rules2').on('change', function () {
    if ($(this).val()) {
      $(this).parent().removeClass('error')
    } else {
      $(this).parent().addClass('error')
    }
  });

});