import $ from 'jquery';
import bodymovin from 'lottie-web';

import lineDirections from './getDirections';
import lineObjectSimple from './linkLine1';
import lineObject from './linkLine2';
import logoObject from './data';

import animals from './icons/animals';
import budget from './icons/budget';
import chillout from './icons/chillout.json';
import data from './icons/data';
import drinks from './icons/drinks.json';
import flexible from './icons/flexible.json';
import holiday from './icons/holiday.json';
import location from './icons/location.json';
import remote_work from './icons/remote_work.json';
import software from './icons/software.json';

import branding from './icons/brand.json';
import web_dev from './icons/web_dev.json';
import mobile_dev from './icons/mobile_dev.json';
import product from './icons/product.json';
import support from './icons/support.json';

import loader from './loader.json';

//buttons
import btn_blue from './btn_blue';
import btn_blue_big from './btn_blue_big';
import btn_yellow from './btn_yellow';


$(document).ready(function () {
        function logo_animation(logo, logoContainer, logoObj) {
            let param = {
                container: logo,
                renderer: "svg",
                loop: false,
                autoplay: true,
                autoloadSegments: false,
                animationData: logoObj
            };

            let anim;
            anim = bodymovin.loadAnimation(param);
            anim.goToAndPlay(0);

            $(logoContainer).on('mouseover', function () {
                if (anim.isPaused) {
                    anim.goToAndPlay(0);
                }
            });
        }

        function loadAnimationAutoPlay(line, lineContainer, lineObject) {
            let parameters = {
                container: line,
                renderer: "svg",
                loop: true,
                autoplay: true,
                animationData: lineObject
            };

            let animation;
            animation = bodymovin.loadAnimation(parameters);
            animation.setDirection(1);
            animation.play();
        }

        function loadAnimation(line, lineContainer, lineObject) {
            let parameters = {
                container: line,
                renderer: "svg",
                loop: false,
                autoplay: false,
                animationData: lineObject
            };

            let animation;
            animation = bodymovin.loadAnimation(parameters);
            $(lineContainer).on('mouseover', function () {
                animation.setDirection(1);
                animation.play();
            });
            $(lineContainer).on('mouseout', function () {
                animation.setDirection(-1);
                animation.play();
            });
        }

        function buttonsAnimation(element, buttonObject) {
            let button_parameters = {
                container: element,
                renderer: "svg",
                loop: false,
                autoplay: false,
                animationData: buttonObject,
                rendererSettings: {
                    className: 'btn__lottie__svg'
                }
            };

            let animation;
            animation = bodymovin.loadAnimation(button_parameters);
            $(element).on('mouseover', function () {
                animation.setDirection(1);
                animation.play();
            });
            $(element).on('mouseout', function () {
                animation.setDirection(-1);
                animation.play();
            });
        }

        function buttonsAnimationBig(element, buttonObject) {
            let button_parameters = {
                container: element,
                renderer: "svg",
                loop: false,
                autoplay: false,
                animationData: buttonObject,
                rendererSettings: {
                    className: 'btn__lottie__svg-big'
                }
            };

            let animation;
            animation = bodymovin.loadAnimation(button_parameters);
            $(element).on('mouseover', function () {
                animation.setDirection(1);
                animation.play();
            });
            $(element).on('mouseout', function () {
                animation.setDirection(-1);
                animation.play();
            });
        }

        let link_line_first = $(".linkLine1");
        link_line_first.each(function (index, element) {
            loadAnimation(element, $(element).parent('.hover_animations'), lineObjectSimple);
        });

        let link_line_second = $(".linkLine2");
        link_line_second.each(function (index, element) {
            loadAnimation(element, $(element).parent('.hover_animations'), lineObject);
        });

        let link_line_directions = $(".linkLineDirections");
        link_line_directions.each(function (index, element) {
            $(element).parent('.hover_animations');
            loadAnimation(element, $(element).parent('.hover_animations'), lineDirections);
        });

        let logo_els = $('.animated_logo');
        logo_els.each(function (index, el) {
            logo_animation(el, $(el).parent('.logo_animations'), logoObject);
        });

        // let loader_el = $('.animated_loader');
        // loader_el.each(function (index, el) {
        //   loadAnimationAutoPlay(el, $(el).parent('.loader_animations'), loader);
        // });

        let icons_career = $('.animated_icon');
        icons_career.each(function (index, el) {
            if ($(this).data('name') != "") {
                if ($(this).data('name') == 'animals') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), animals);
                } else if ($(this).data('name') == 'budget') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), budget);
                } else if ($(this).data('name') == 'chillout') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), chillout);
                } else if ($(this).data('name') == 'data') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), data);
                } else if ($(this).data('name') == 'drinks') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), drinks);
                } else if ($(this).data('name') == 'flexible') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), flexible);
                } else if ($(this).data('name') == 'holiday') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), holiday);
                } else if ($(this).data('name') == 'location') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), location);
                } else if ($(this).data('name') == 'remote_work') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), remote_work);
                } else if ($(this).data('name') == 'software') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), software);
                } else if ($(this).data('name') == 'product') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), product);
                } else if ($(this).data('name') == 'web_dev') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), web_dev);
                } else if ($(this).data('name') == 'branding') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), branding);
                } else if ($(this).data('name') == 'mobile_dev') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), mobile_dev);
                } else if ($(this).data('name') == 'support') {
                    loadAnimationAutoPlay(el, $(el).parent('.icon_animations'), support);
                }
            }
        });


        let button_yellow = $('.btn__lottie--content');
        button_yellow.each(function (index, el) {
            buttonsAnimation(el, btn_yellow);
        });

        let button_blue = $('.btn__lottie--bg');
        button_blue.each(function (index, el) {
            buttonsAnimation(el, btn_blue);
        });

        let button_blue_big = $('.btn__lottie--bg-big');
        button_blue_big.each(function (index, el) {
            buttonsAnimationBig(el, btn_blue);
        });

    }
);
