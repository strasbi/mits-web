<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

require get_template_directory() . '/include/Benefits.php';
$benefits = new Benefits;
$context['benefits'] = $benefits->getBenefits();

if ('post' == get_post_type()) {
    $categories = get_the_category();
    $context['categories'] = array();
    foreach ($categories as $cat) {
        $category = array();
        $category['link'] = get_category_link($cat->term_id);
        $category['name'] = $cat->name;
        array_push($context['categories'], $category);
    }

    $args = array(
        'numberposts' => 5,
        'cat' => $categories[0]->term_id,
        'post_type' => 'post',
        'post__not_in' => array( $post->ID )
    );

    $context['related_posts'] = Timber::get_posts($args);

    $categories = get_categories();
    $context['all_categories'] = array();
    foreach ($categories as $cat) {
        $category = array();
        $category['link'] = get_category_link($cat->term_id);
        $category['name'] = $cat->name;
        array_push($context['all_categories'], $category);
    }
    $author_id = get_the_author_meta('ID');
    $context['author']['name'] = get_the_author_meta('display_name');
    if(get_field('image', 'user_'.$author_id) != "") {
        $context['author']['avatar'] = get_field('image', 'user_'.$author_id);
    } else {
        $context['author']['avatar'] = get_avatar_url($author_id);
    }
    $context['author']['desc'] = get_the_author_meta('description');
    $context['author']['job'] = get_field('job', 'user_'.$author_id);

    $context['post_date'] = get_the_date( 'd/m/Y' );


    $mycontent = $post->post_content;
    $word = str_word_count(strip_tags($mycontent));
    $minute = floor($word / 200);
    $context['time_estimate'] = $minute ;


    $context["previous_post"] = get_previous_post();
    if ($context["previous_post"] == "") {
        $all_posts = get_posts(array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'order' => 'DESC'
        ));
        $context["previous_post"] = $all_posts[0];
    }

    $context["next_post"] = get_next_post();
    if ($context["next_post"] == "") {
        $all_posts = get_posts(array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'order' => 'ASC'
        ));
        $context["next_post"] = $all_posts[0];
    }


} elseif ('services' == get_post_type()) {

    $case_study_posts = get_field('case_studies');
    if (is_array($case_study_posts)) {
        foreach ($case_study_posts as $index => $single_case) {
            $context['case_studies'][$index] = get_field('case_studies_header', $single_case->ID);
            $context['case_studies'][$index]['link'] = get_permalink($single_case->ID);

            $negPosArray = [-1, 1];
            $depth = rand(20, 60) / 100 * $negPosArray[rand(0, 1)];
            $frictionX = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
            $frictionY = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
            $context['case_studies'][$index]['parallaxOpts'] = [
                'depth' => $depth,
                'frictionX' => $frictionX,
                'frictionY' => $frictionY,
            ];
        }
    }


    $terms = get_the_terms($post, 'services_cat');
    if (is_array($terms)) {
        $term = $terms[0];
    }

    $services_cat = get_terms('services_cat', array(
        'hide_empty' => false,
    ));
    $context['services_cat'] = array();
    foreach ($services_cat as $cat) {
        $service_tmp = array();
        $service_tmp['name'] = $cat->name;
        $service_tmp['link'] = get_term_link($cat);
        if ($term) {
            if ($term == $cat) {
                $service_tmp['active'] = true;
            } else {
                $service_tmp['active'] = false;
            }
        }
        array_push($context['services_cat'], $service_tmp);
    }


    $context["previous_post"] = get_previous_post();
    if ($context["previous_post"] == "") {
        $all_posts = get_posts(array(
            'post_type' => 'services',
            'posts_per_page' => -1,
            'order' => 'DESC'
        ));
        $context["previous_post"] = $all_posts[0];
    }

    $context["next_post"] = get_next_post();
    if ($context["next_post"] == "") {
        $all_posts = get_posts(array(
            'post_type' => 'services',
            'posts_per_page' => -1,
            'order' => 'ASC'
        ));
        $context["next_post"] = $all_posts[0];
    }

    $context['layout_form']['acf_fc_layout'] = "contact_form";
    $context['layout_form']['button_text'] = __("Let's talk", "mits");
}

if (post_password_required($post->ID)) {
    Timber::render('single-password.twig', $context);
} else {
    Timber::render(array('single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig'),
        $context);
}
