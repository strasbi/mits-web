<?php

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite
{

    function __construct()
    {
        add_theme_support('post-formats');
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'));
        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));
        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        add_action('rest_api_init', array($this, 'register_rest_routes'));
        parent::__construct();
    }

    function register_post_types()
    {
//        register_post_type('PostType',
//            array(
//                'labels' => array(
//                    'name' => __('PostType'),
//                    'singular_name' => __('PostType')
//                ),
//                'taxonomies' => array('PostTypeTax'),
//                'public' => true,
//                'has_archive' => true,
//                'rewrite' => array('slug' => 'PostType'),
//                'capability_type' => 'post',
//                'publicly_queryable' => true,
//                'description' => 'PostType',
//                'supports' => array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'),
//                'taxonomy' => array('category'),
//                'show_in_admin_all_list' => true,
//                'show_in_admin_status_list' => true,
//                'show_in_rest' => false,
//                'menu_icon' => 'dashicons-calendar-alt'
//            )
//        );
        register_post_type('careers',
            array(
                'labels' => array(
                    'name' => __('careers'),
                    'singular_name' => __('career')
                ),
                'taxonomies' => array('careers_cat'),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'careers'),
                'capability_type' => 'post',
                'publicly_queryable' => true,
                'description' => 'PostType',
                'supports' => array('title', 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'),
                'taxonomy' => array('category'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-businesswoman'
            )
        );
        register_post_type('case_studies',
            array(
                'labels' => array(
                    'name' => __('Case studies'),
                    'singular_name' => __('Case')
                ),
                'taxonomies' => array('case_studies_cat'),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'case-studies'),
                'capability_type' => 'post',
                'publicly_queryable' => true,
                'description' => 'PostType',
                'supports' => array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category', 'title'),
                'taxonomy' => array('category'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-format-aside'
            )
        );

        register_post_type('inspired_titles',
            array(
                'labels' => array(
                    'name' => __('Inspired titles'),
                    'singular_name' => __('inspired_titles')
                ),
                'taxonomies' => array('inspired_titles_cat'),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => 'inspired_titles'),
                'capability_type' => 'post',
                'publicly_queryable' => false,
                'description' => 'PostType',
                'supports' => array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category', 'title'),
                'taxonomy' => array('category'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-format-aside'
            )
        );
        register_post_type('testimonials',
            array(
                'labels' => array(
                    'name' => __('Testimonials', 'mits'),
                    'singular_name' => __('Testimonials', 'mits')
                ),
                'public' => true,
                'has_archive' => false,
                'capability_type' => 'post',
                'publicly_queryable' => false,
                'description' => 'PostType',
                'supports' => array('page-attributes', 'title', 'caption',),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-businessman'
            )
        );

        register_post_type('services',
            array(
                'labels' => array(
                    'name' => __('Services', 'mits'),
                    'singular_name' => __('Service', 'mits')
                ),
                'taxonomies' => array('services_cat'),
                'public' => true,
                'hierarchical' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'services'),
                'capability_type' => 'post',
                'publicly_queryable' => true,
                'description' => 'PostType',
                'supports' => array('page-attributes', 'title', 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'),
                'taxonomy' => array('services_cat'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-cart'
            )
        );
        register_post_type('benefits',
            array(
                'labels' => array(
                    'name' => __('benefits'),
                    'singular_name' => __('benefit')
                ),
                'public' => true,
                'hierarchical' => false,
                'has_archive' => false,
                'capability_type' => 'post',
                'publicly_queryable' => false,
                'description' => 'PostType',
                'supports' => array('page-attributes', 'title', 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => false,
                'menu_icon' => 'dashicons-cart'
            )
        );
    }

    function register_taxonomies()
    {
//        register_taxonomy(
//            'PostType_category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
//            'PostType',        //post type name
//            array(
//                'hierarchical' => true,
//                'label' => 'Kategorie pojęć',  //Display name
//                'query_var' => true,
//                'rewrite' => array(
//                    'slug' => 'PostType', // This controls the base slug that will display before each term
//                    'with_front' => false // Don't display the category base before
//                )
//            )
//        );
        register_taxonomy(
            'careers_cat',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'careers',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Category for careers',  //Display name
                'query_var' => true,
                'public' => false,
                'show_ui' => true,
                'rewrite' => array(
                    'slug' => 'careers', // This controls the base slug that will display before each term
                    'with_front' => false // Don't display the category base before
                )
            )
        );
        register_taxonomy(
            'case_studies_cat',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'case_studies',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Category for case studies',  //Display name
                'query_var' => true,
                'public' => false,
                'show_ui' => true,
                'rewrite' => array(
                    'slug' => 'Category for case studies', // This controls the base slug that will display before each term
                    'with_front' => false // Don't display the category base before
                )
            )
        );
        register_taxonomy(
            'services_cat',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'services',        //post type name
            array(
                'hierarchical' => true,
                'label' => __('Category for Services', 'mits'),  //Display name
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'services-categories', // This controls the base slug that will display before each term
                    'with_front' => false // Don't display the category base before
                )
            )
        );
    }

    function register_nav()
    {
        register_nav_menu('primary', __('Primary Menu', 'top-menu'));
        register_nav_menu('footer', __('Footer Menu', 'footer-menu'));
    }

    function register_rest_routes()
    {
        register_rest_route('demo', '/demo', array(
            'methods' => 'GET',
            'callback' => function ($data) {
                $posts = "";
                return $posts;
            },
        ));
    }

    function add_to_context($context)
    {
        if (is_array(get_fields())) {
        	$counter = 0;
            foreach (get_fields() as $key => $field) {
                if (count((array)$field) && is_array($field)) {
                    $layouts = array_filter($field, function ($item) {
                        if (!is_array($item)) return false;
                        return array_key_exists('acf_fc_layout', $item);
                    });

                    if (count($layouts)) {
                        foreach ($layouts as $index => $layout) {
                            if ($layout['acf_fc_layout'] == "main_slider") {
                                $taxonomy_slider = array();
                                if (array_key_exists('taxonomy_services', $layout)) {
                                    if ($layout['taxonomy_services']) {
                                        foreach ($layout['taxonomy_services'] as $term) {
                                            $args = array(
                                                'post_type' => 'services',
                                                'posts_per_page' => -1,
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'services_cat',
                                                        'field' => 'term_id',
                                                        'terms' => $term->term_id,
                                                    )
                                                )
                                            );
                                            $taxonomy_posts = Timber::get_posts($args);
                                            if (have_rows('repeater_services', $term)):
                                                while (have_rows('repeater_services', $term)) : the_row();
                                                    $name = get_sub_field('service_name');
                                                    $service = array();
                                                    $service['post_title'] = $name;
                                                    $service['repeater'] = true;
                                                    array_push($taxonomy_posts, $service);
                                                endwhile;
                                            endif;
                                            array_push($taxonomy_slider, $taxonomy_posts);
                                        }
                                        $context["taxonomy_slider"] = $taxonomy_slider;
                                    }
                                }
                            }
//                            $context['layouts'][$index] = $layout;
                            $context['layouts'][$counter++] = $layout;
                        }
                    }
                }
            }
        }

        $context['title'] = get_the_title(get_the_ID());
        $context['postID'] = get_the_ID();
        if (is_home() || is_front_page()) {
            $context['home'] = true;
        } else {
            $context['home'] = true;
        }

        $custom_logo_id = get_theme_mod('custom_logo');
        $image = wp_get_attachment_image_src($custom_logo_id, 'full');
        $context['logo'] = $image[0];
//        $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
        $context['home_url'] = apply_filters('wpml_home_url', get_option('home'));
        $context['current_lang'] = ICL_LANGUAGE_CODE;

        $context['lang_ico'] = (ICL_LANGUAGE_CODE === 'pl') ?
            (array_key_exists('en', icl_get_languages()) ? icl_get_languages()['en'] : icl_get_languages()['pl'] ) :
            icl_get_languages()['pl']
        ;

        $header_group = get_field('header-bg', 'options');
        if ($header_group) {
            $context['header_mobile'] = $header_group['header-bg-mobile'];
            $context['header_desktop'] = $header_group['header-bg-desktop'];
        }
        $context['header_title'] = get_field('header-title', 'option');
        $context['header_desc'] = get_field('header-desc', 'option');
        $context['header_btn'] = get_field('header-btn', 'option');

        $context['header_brands'] = get_field('header_brands', 'option');

        $context['site_title'] = get_bloginfo('name');

        $context['footer']['disable'] = get_field('disable_footer');
        $context['footer']['data'] = get_field('comapny_data', 'options');
        $context['footer']['contact'] = get_field('contact-to-company', 'options');
        $context['footer']['social_media'] = get_field('company_social_media', 'options');
        $context['footer']['cookies_title'] = get_field('cookies_title', 'options');
        $context['footer']['cookies_text'] = get_field('cookies_text', 'options');
        $context['footer']['location'] = get_field('location_group', 'options');
        $context['general']['services_slider'] = get_field('services_slider', 'options');
        $context['logo_reverse'] = get_theme_mod('logo_reverse');
        $context['foo'] = 'bar';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        $context['template_url'] = get_template_directory_uri();
        $context['captcha_site_key'] = get_field("catpcha_site_key", "option");


        $args_careers = array(
            'post_type' => 'careers',
            'post_status' => 'publish',
            'posts_per_page' => -1,
	        'tax_query' => [
	        	[
	        		'taxonomy' => 'careers_cat',
			        'field' => 'name',
			        'terms' => ['Ongoing', 'W toku']
		        ]
	        ]
        );

        $posts_careers = get_posts($args_careers);
        $context['careers_count'] = count($posts_careers);


        $args_inspired = array(
            'post_type' => 'inspired_titles',
            'posts_per_page' => -1,
            'suppress_filters' => false
        );

        $posts_inspired = get_posts($args_inspired);
        foreach ($posts_inspired as $key => $single) {
            $context['inspired_titles'][$key]['text'] = $single->post_title;
        }

        $context['ignore_gtm'] = false;
        if (isset($_COOKIE['ignoreGA']) && $_COOKIE['ignoreGA'] === 'ignore') {
            $context['ignore_gtm'] = true;
        }

        $this->recentBlogPosts($context);

        return $context;
    }

    function myfoo($text)
    {
        $text .= ' bar!';
        return $text;
    }

    function add_to_twig($twig)
    {
        /* this is where you can add your own functions to twig */
        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
        return $twig;
    }

    private function recentBlogPosts(array &$context) {
        $args_posts = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3
        );

        $posts = get_posts($args_posts);
        $context['recent_posts'] = array_map(function ($post) {
            return new Timber\Post($post->ID);
        }, $posts);
    }
}


new StarterSite();


$composer_autoload = __DIR__ . '/vendor/autoload.php';
require_once($composer_autoload);
//require get_template_directory() . '/include/Geolocation.php';

function register_my_session()
{
    if (!session_id()) {
        session_start();
    }
    $geolocation = new Geolocation;
    $geolocation->wpRedirect();
}

//add_action('init', 'register_my_session');


function theme_styles()
{
    wp_enqueue_style('main_css', get_template_directory_uri() . '/dist/main.css');
}

add_action('wp_enqueue_scripts', 'theme_styles');

function theme_js()
{
    global $wp_query;
    global $wp_scripts;

    $site_key = get_field("catpcha_site_key", "option");
    /* if ((is_single() && 'post' == get_post_type()) || is_archive() || get_the_title() == "Blog") {
        wp_enqueue_script('mootools', plugins_url() . '/enlighter/resources/mootools-core-yc.js', array(), '3.3', true);
        wp_enqueue_script('EnlighterJS', plugins_url() . '/enlighter/resources/EnlighterJS.min.js', array(), '3.3', true);
    } */
    wp_enqueue_script('wow_js', get_template_directory_uri() . '/src/scripts/wow/wow.js');
    wp_enqueue_script('my_custom_js', get_template_directory_uri() . '/dist/main.js');
    wp_enqueue_script('google_captcha', 'https://www.google.com/recaptcha/api.js?render='.$site_key);
//    wp_enqueue_script('wow_js_exec', get_template_directory_uri() . '/src/scripts/wowwow.js');
    wp_localize_script('my_custom_js', 'dynamic_scripts',
        array('theme_url' => get_template_directory_uri(),
            'ajax' => admin_url('admin-ajax.php'),
            'posts' => json_encode($wp_query->query_vars), // everything about your loop is here
            'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages
        )
    );
}

add_action('wp_enqueue_scripts', 'theme_js');

add_action('wp', 'create_layout_file_if_not_exist');
function create_layout_file_if_not_exist()
{
    if (is_admin()) return;
    if (is_array(get_fields())) {
        foreach (get_fields() as $flex) {
            if (is_array($flex)) {
                foreach ($flex as $flexEl) {
                    if (is_array($flexEl) && array_key_exists('acf_fc_layout', $flexEl)) {
                        $layout_dir = get_template_directory() . '/templates/layouts/';
                        $filename = $layout_dir . 'layout-' . $flexEl['acf_fc_layout'] . '.twig';
                        if (!file_exists($filename)) {
//                            echo $filename . '</br>';
//                            echo 'Tworzę plik: ' . $filename;
                            copy($layout_dir . 'layout-default.twig', $filename); //kopiujemy plik z defaut
                            exec('chmod 0666 ' . $filename);
                            chmod($filename, 0666);
                        }
                    }
                }
            }
        }
    }
}

//includes additional functions

//reuqire
require get_template_directory() . '/include/sec.php';
require get_template_directory() . '/include/captcha.php';
//include customizer options
require get_template_directory() . '/include/customizer.php';
require get_template_directory() . '/include/dynamic_contact.php';
require get_template_directory() . '/include/estimate.php';

require get_template_directory() . '/include/join_us_contact.php';
require get_template_directory() . '/include/contact.php';
require get_template_directory() . '/include/load_more_posts.php';
require get_template_directory() . '/include/works_list.php';

if (function_exists('acf_add_options_page')) {

    // add parent
    $parent = acf_add_options_page(array(
        'page_title' => 'Ustawienia motywu',
        'menu_title' => 'Ustawienia motywu',
        'menu_slug' => 'theme option',
        'position' => 25,
        'redirect' => true
    ));
    // add sub page
    acf_add_options_sub_page(array(
        'page_title' => 'header',
        'menu_title' => 'header',
        'menu_slug' => 'header',
        'parent_slug' => 'theme option',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'footer',
        'menu_title' => 'footer',
        'menu_slug' => 'footer',
        'parent_slug' => 'theme option',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'general',
        'menu_title' => 'general',
        'menu_slug' => 'general',
        'parent_slug' => 'theme option',
    ));
}


function theme_prefix_setup()
{
    add_theme_support('custom - logo', array(
        'height' => 100,
        'width' => 400,
        'flex - width' => true,
    ));

}

add_action('after_setup_theme', 'theme_prefix_setup');


//export acf fields
add_filter('acf/settings/ save_json', 'ks_acf_json_save_point');
function ks_acf_json_save_point($path)
{
    $path = get_template_directory() . ' /acf-json';
    return $path;
}

//import acf fields
add_filter('acf/settings/load_json', 'ks_acf_json_load_point');
function ks_acf_json_load_point($paths)
{
    unset($paths[0]);
    $paths[] = get_template_directory() . ' /acf-json';
    return $paths;
}


//acf google maps init
//function my_acf_init()
//{
//    acf_update_setting('google_api_key', 'AIzaSyDSfpYcaPMy5OWkkJZM3ZxrZUi9BInsUqY & callback');
//}
//add_action('acf / init', 'my_acf_init');


/*
* Add columns to exhibition post list
*/
//function add_acf_columns($columns)
//{
//    return array_merge($columns, array(
//        'training - date' => __('Data wydarzenia'),
//    ));
//}
//add_filter('manage_events_posts_columns', 'add_acf_columns');

/*
 * Add columns to exhibition post list
 */
//function events_custom_column($column, $post_id)
//{
//    switch ($column) {
//        case 'training - date':
//            echo sprintf(' % s', date('d / m / Y', strtotime(get_post_meta($post_id, 'training - date', true))));
//            break;
//    }
//}
//add_action('manage_events_posts_custom_column', 'events_custom_column', 10, 2);

//remove default POSTS from main wp bar
//add_action('admin_menu', 'remove_default_post_type');
//function remove_default_post_type()
//{
//    remove_menu_page('edit . php');
//}

//enable files
function my_custom_mime_types($mimes)
{

// New allowed mime types.
    $mimes['svg'] = 'image / svg + xml';
//    $mimes['svgz'] = 'image / svg + xml';
//    $mimes['doc'] = 'application / msword';

// Optional. Remove a mime type.
    unset($mimes['exe']);

    return $mimes;
}

add_filter('upload_mimes', 'my_custom_mime_types');


function get_svg__with_hover_color_from_file($file, $color)
{
    $svg = file_get_contents($file);
    $svg = str_replace(' <svg', ' <svg class="social__svg" data-hover-color = "' . $color . '"', $svg);
    $svg = str_replace(' <path', ' <path class="social__svg__path" data-hover-color = "' . $color . '"', $svg);
    return $svg;
}

add_filter('timber/twig', 'add_to_twig');

function add_to_twig($twig)
{
    $twig->addFilter(new Timber\Twig_Filter('removeOrphans', 'removeOrphans'));
    return $twig;
}


function removeOrphans($origText)
{
    $remove = array(
        ' / w / ',
        ' / W / ',
        ' / z / ',
        ' / Z / ',
        ' / o / ',
        ' / O / ',
        ' / i / ',
        ' / I / ',
        ' / a / ',
        ' / A / ',
    );
    $insert = array(
        ' w &nbsp;',
        ' W &nbsp;',
        ' z &nbsp;',
        ' Z &nbsp;',
        ' o &nbsp;',
        ' O &nbsp;',
        ' i &nbsp;',
        ' I &nbsp;',
        ' a &nbsp;',
        ' A &nbsp;',
    );
    $text = preg_replace($remove, $insert, $origText);
    return $text;
}
//disable video controls
add_filter('wp_video_shortcode', function ($output) {
    $output = str_replace('controls = "controls"', '', $output);
    return $output;
});
add_action('template_redirect', 'mits_disable_author_page');
// HIDE AUTHOR PAGE
function mits_disable_author_page() {
	global $wp_query;
	if ( is_author() ) {
		// Redirect to homepage, set status to 301 permenant redirect.
		// Function defaults to 302 temporary redirect.
		wp_redirect(get_option('home'), 301);
		exit;
	}
}