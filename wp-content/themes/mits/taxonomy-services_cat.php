<?php

use Timber\Term;

$templates = array('archive.twig', 'index.twig');

$context = Timber::get_context();

$term = get_queried_object();


$context['term_page'] = new Timber\Term();
$context['layouts'] = $context['term_page']->sections;


$context['layout_form']['acf_fc_layout'] = "contact_form";
$context['layout_form']['button_text'] = __("Let's talk", "mits");

$posts_list_args = array(
    'post_type' => 'services',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'services_cat',
            'field' => 'term_id',
            'terms' => $term->term_id,
        )
    )
);

$services_cat = get_terms('services_cat', array(
    'hide_empty' => false,
));
$context['services_cat'] = array();
foreach ($services_cat as $cat) {
    $service_tmp = array();
    $service_tmp['name'] = $cat->name;
    $service_tmp['link'] = get_term_link($cat);
    if ($term == $cat) {
        $service_tmp['active'] = true;
    } else {
        $service_tmp['active'] = false;
    }
    array_push($context['services_cat'], $service_tmp);
}

$case_study_posts = get_field('case_studie', $term);

if($case_study_posts) {
    foreach ($case_study_posts as $index => $single_case) {
        $context['case_studies_service'][$index] = get_field('case_studies_header', $single_case->ID);
        $context['case_studies_service'][$index]['link'] = get_permalink($single_case->ID);

        $negPosArray = [-1, 1];
        $depth = rand(20, 60) / 100 * $negPosArray[rand(0, 1)];
        $frictionX = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
        $frictionY = rand(200, 600) / 1000 * $negPosArray[rand(0, 1)];
        $context['case_studies_service'][$index]['parallaxOpts'] = [
            'depth' => $depth,
            'frictionX' => $frictionX,
            'frictionY' => $frictionY,
        ];


        if (have_rows('case_studies_flexible', $single_case->ID)):
            while (have_rows('case_studies_flexible', $single_case->ID)) : the_row();
                if(get_row_layout() == 'case_studies_result'):
                    $context['case_studies_service'][$index]['layout_case_studies_result']['acf_fc_layout'] = "case_studies_result";
                    $context['case_studies_service'][$index]['layout_case_studies_result']['about'] = get_sub_field('about');
                    $context['case_studies_service'][$index]['layout_case_studies_result']['image'] = get_sub_field('image');
                    $context['case_studies_service'][$index]['layout_case_studies_result']['name'] = get_sub_field('name');
                    $context['case_studies_service'][$index]['layout_case_studies_result']['position'] = get_sub_field('position');
                    $context['case_studies_service'][$index]['layout_case_studies_result']['background'] = get_sub_field('background');
                    $context['case_studies_service'][$index]['layout_case_studies_result']['section_number'] = get_sub_field('section_number');
                endif;
            endwhile;
        endif;

    }
}


$context['services'] = Timber::get_posts($posts_list_args);
if (have_rows('repeater_services', $term)):
    while (have_rows('repeater_services', $term)) : the_row();
        $name = get_sub_field('service_name');
        $service = array();
        $service['post_title'] = $name;
        $service['repeater'] = true;
        array_push($context['services'], $service);
    endwhile;
endif;

$context['title'] = $term->name;

array_unshift($templates, 'taxonomy-' . $term->taxonomy . '.twig');


Timber::render($templates, $context);