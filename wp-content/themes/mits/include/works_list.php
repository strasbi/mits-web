<?php
function loadmore_works_ajax_handler()
{


    $args = array(
        'post_type' => 'case_studies',
        'posts_per_page' => -1,
        'offset' => esc_sql($_POST['offset']),
        'tax_query' => [
            [
                'taxonomy' => 'case_studies_cat',
                'terms' => esc_sql($_POST['cat']),
            ],
        ],
    );

    $case_studies = Timber::get_posts($args);
    $output = '';
    if (count($case_studies) > 0) {
        $output = Timber::compile('tease-works.twig', array('case_studies' => $case_studies));
        wp_send_json_success(array('html' => $output, 'count' => count($case_studies) ));
    } else {
        wp_send_json_error(__("Brak postów"));
    }

}


add_action('wp_ajax_loadmore_works', 'loadmore_works_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore_works', 'loadmore_works_ajax_handler'); // wp_ajax_nopriv_{action}
