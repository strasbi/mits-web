<?php

add_action('wp_ajax_join_us_contact_form', 'join_us_form'); //
add_action('wp_ajax_nopriv_join_us_contact_form', 'join_us_form');

function join_us_form()
{
    if(!verify_captcha($_POST['g-recaptcha-response'])) {
        wp_send_json_error(__("You are a Robot!!", 'mits'));
    }
    $data = esc_sql(maybe_unserialize($_POST));
    $return_data = [];
    $return_error = [];
    if (!$data) {
        wp_send_json_error("cant read post data");
    } else {
        foreach ($data as $key => $single_field) {
            if ($key !== 'cv' && ($single_field === NULL || $single_field === '')) {
                $return_error[$key] = __('This field is required', 'mits');

            } else {
                $return_data[$key] = $single_field;
            }
        }
        if (!preg_match("/@.+\./", $data['email'])) {
            $return_error['email'] = __('Invalid email address', 'mits');
        }
        if ($return_error) {
            wp_send_json_error($return_error);
        } else {
            if ($_FILES['cv']['name'] != "") {
                $allowed = array('pdf', 'PDF', 'doc', 'docx');
                $filename = $_FILES['cv']['name'];
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $return_error['cv'] = __('Wrong file format', 'mits');
                    wp_send_json_error($return_error);
                }
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                require_once(ABSPATH . 'wp-admin/includes/file.php');
                require_once(ABSPATH . 'wp-admin/includes/media.php');

                $uploadedfile = $_FILES['cv'];
                $upload_overrides = array(
                    'test_form' => false
                );
                $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
                if ($movefile && !isset($movefile['error'])) {


                    $subject = "[JOIN MITS] " . $return_data['title'];
                    $to = "hello@mits.pl";
                    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Formularz mits  <formularz@mits.pl>', 'Reply-To: <' . $to . '>');
                    $body = '';
                    foreach ($return_data as $key => $value) {
                        $body .= $key . ': ' . $value . '<br/>';
                    }
                    $send_mail = wp_mail($to, $subject, $body, $headers, $movefile);
                    unlink($movefile['file']);
                    if ($send_mail) {
                        wp_send_json_success(__("Your message has been sent.", 'mits'));
                    } else {
                        wp_send_json_error(__("Couldn't send this message", 'mits'));
                    }
                } else {
                    wp_send_json_error(__("Couldn't send this message", 'mits'));
                }

            } else {
                $subject = "[JOIN MITS] " . $return_data['title'];
                $to = "hello@mits.pl";
                $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Formularz mits <formularz@mits.pl>', 'Reply-To: <' . $to . '>');
                $body = '';

                foreach ($return_data as $key => $value) {
                    $body .= $key . ': ' . $value . '<br/>';
                }

                $send_mail = wp_mail($to, $subject, $body, $headers);

                if ($send_mail) {
                    wp_send_json_success(__("Your message has been sent.", 'mits'));
                } else {
                 //   debug_wpmail($send_mail);
                    wp_send_json_error(__("Couldn't send this message", 'mits'));
                }
            }
        }
    }
}
