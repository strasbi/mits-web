<?php


add_action('wp_ajax_estimate', 'estimate'); //
add_action('wp_ajax_nopriv_estimate', 'estimate');

function estimate()
{

    if(!verify_captcha($_POST['g-recaptcha-response'])) {
        wp_send_json_error(__("You are a Robot!!", 'mits'));
    }
    $data = esc_sql(maybe_unserialize($_POST));
    $return_data = [];
    if ($data) {
        if (!$data['name']) {
            $return_data['error']['name'] = __('name is required', 'mits');
        }
        if (!$data['email']) {
            $return_data['error']['email'] = __('email is required', 'mits');
        }
        if (!$data['phone']) {
            $return_data['error']['phone'] = __('phone is required', 'mits');
        }
//        if (!$data['where']) {
//            $return_data['error']['where'] = __('How did you hear about us we wont to know!', 'mits');
//        }
//        if (!$data['privacy_terms'] || !$data['rambo']) {
//            $return_data['error']['terms'] = __('You should accept ours terms! Do it now men!', 'mits');
//        }
        if (!preg_match("/@.+\./", $data['email'])) {
            $return_data['error']['email'] = __('invalid data', 'mits');
        }
        if (!$data['nda']) {
            $data['nda'] = __('no no no', 'mits');
        }
        $attachment = "";

        if ($_FILES['file']['name'] != "") {
            $allowed = array('pdf', 'PDF', 'doc', 'docx');
            $filename = $_FILES['file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                $return_data['error']['file'] = __('Wrong file format', 'mits');
            } else {
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                require_once(ABSPATH . 'wp-admin/includes/file.php');
                require_once(ABSPATH . 'wp-admin/includes/media.php');

                $uploadedfile = $_FILES['file'];
                $upload_overrides = array(
                    'test_form' => false
                );
                $attachment = wp_handle_upload($uploadedfile, $upload_overrides);
            }
        }


        $scope_text = "";
        if( count($data['scopes']) > 0 ) {
            foreach ($data['scopes'] as $scope) {
                $scope_text .= $scope.', ';
            }
        }
        if (!$return_data['error']) {
//            wp_send_json_success("its ok");
            $to = "hello@mits.pl";
            $subject = 'Get an estimate from mits.pl';
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Formularz mits.pl <formularz@mits.pl>', 'Reply-To: <' . $to . '>');
            $message = "NAME: " . $data['name'] . "<br />" . "EMAIL: " . $data['email'] . "<br />" . "TELEPHONE: " . $data['phone'] .  "<br />" . "SCOPE: " . $scope_text . "<br />" . "WHERE DID YOU HEAR ABOUT MITS: " . $data['where'] . "<br />" . "ABOUT PROJECT: " . $data['about_project'] . "<br />" . "NDA?" . $data['nda'];
           // wp_send_json_success(array($message, $attachment));

            if (!function_exists('debug_wpmail')) :
                function debug_wpmail($result = false)
                {
                    if ($result)
                        return;
                    global $ts_mail_errors, $phpmailer;
                    if (!isset($ts_mail_errors))
                        $ts_mail_errors = array();
                    if (isset($phpmailer))
                        $ts_mail_errors[] = $phpmailer->ErrorInfo;
//                    print_r('<pre>');
//                    print_r($ts_mail_errors);
//                    print_r('</pre>');
                }
            endif;

            if ($attachment && $attachment != "" && !isset($attachment['error'])) {
                $send_mail = wp_mail($to, $subject, $message, $headers, $attachment);
                unlink($attachment['file']);
            } else {
                $send_mail = wp_mail($to, $subject, $message, $headers);
            }
            if ($send_mail) {
                wp_send_json_success(__("Your message has been sent.", 'mits'));
            } else {
                debug_wpmail($send_mail);
                wp_send_json_error(__("Couldn't send this message", 'mits'));
            }

        }
        wp_send_json_error($return_data);
    } else {
        wp_send_json_error(__("cannot read data", 'mits'));
    }
}
