<?php


class Benefits
{
    private function getBenefitsPosts()
    {
        global $sitepress;
     //   $sitepress->switch_lang($_SESSION['user_lang_code'], true);

        $benefits_args = array(
            'post_type' => 'benefits',
            'posts_per_page' => -1,
      //      'language_code' => $_SESSION['user_lang_code'],
            'suppress_filters' => false
        );
        $benefits_posts = new WP_Query($benefits_args);
        return $benefits_posts->posts;
    }

    private function getBenefitsFields()
    {
        $benefits_arr = [];
        foreach ($this->getBenefitsPosts() as $key => $benefit) {
            $benefits_arr[$key]['name'] = get_the_title($benefit->ID);
            $benefits_arr[$key]['icon'] = get_field('cpt_benefits_image', $benefit->ID);
            $benefits_arr[$key]['data_name'] = get_field('cpt_benefits_name', $benefit->ID);
        }
        return $benefits_arr;
    }

    public function getBenefits()
    {
        return $this->getBenefitsFields();
    }

}