<?php

function verify_captcha($captcha) {
    $Return = getCaptcha($captcha);
    if($Return->success == true && $Return->score > 0.5){
        return true;
    }else{
       return false;
    }
}
function getCaptcha($captcha){
    $SECRET_KEY = get_field("captcha_secret_key", "option");
    $Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$SECRET_KEY."&response={$captcha}");
    $Return = json_decode($Response);
    return $Return;
}