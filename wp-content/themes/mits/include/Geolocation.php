<?php

class Geolocation
{
    private function getUserIP()
    {
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
                $addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    private function getUserCountry()
    {
        if (!isset($_SESSION['user_lang_code'])) {
            $dbFile = get_template_directory() . '/include/databases/IP2LOCATION-LITE-DB11.BIN';
            $db = new \IP2Location\Database($dbFile, \IP2Location\Database::FILE_IO);
            $records = $db->lookup($this->getUserIP(), \IP2Location\Database::ALL);
            $_SESSION['user_lang_code'] = $records['countryCode'];
            return $_SESSION['user_lang_code'];
        } else {
            return $_SESSION['user_lang_code'];
        }
    }

    private function shouldRedirect()
    {
        return !isset($_SESSION['user_lang_code']);
    }

    public function wpRedirect()
    {
        $languages = apply_filters('wpml_active_languages', NULL, 'orderby=id&order=desc');
        $url = $languages['en']['url'];
        if ($this->shouldRedirect() && $this->getUserCountry() !== "PL" && ICL_LANGUAGE_CODE !== 'en') {
            header('Location: ' . $url);
            exit;
        }
    }
}