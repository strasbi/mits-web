<?php

function loadmore_ajax_handler(){


    $posts_list_args = array(
        'post_type' => 'post',
        'posts_per_page' => 1,
        'offset' => esc_sql($_POST['offset'])
    );
    if(esc_sql($_POST['cat']) != "all") {
        $posts_list_args['category'] = esc_sql($_POST['cat']);
    }
    $posts_list = get_posts($posts_list_args);

    if(count($posts_list) > 0) {
        $output = '';
        foreach ($posts_list as $post) {
            $post->link = get_page_link($post->ID);
            $output = Timber::compile('tease-post.twig', array('post' => $post));
        }
        wp_send_json_success($output);
    } else {

    }  wp_send_json_error(__("Brak postów"));

}



add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}