<?php

add_action('wp_ajax_contact_form', 'contact_form'); //
add_action('wp_ajax_nopriv_contact_form', 'contact_form');

function contact_form()
{
    if(!verify_captcha($_POST['g-recaptcha-response'])) {
        wp_send_json_error(__("You are a Robot!!", 'mits'));
    }
    $data = esc_sql(maybe_unserialize($_POST));
    $return_data = [];
    if ($data) {
        if (!$data['name']) {
            $return_data['error']['name'] = __('name is required', 'mits');
        }
        if (!$data['email_or_number']) {
            $return_data['error']['email_or_number'] = __('email/phone is required', 'mits');
        }
        if (!$data['message']) {
            $return_data['error']['message'] = __('message is required', 'mits');
        }

        if (!$data['email_or_number']) {
            $return_data['error']['email_or_number'] = __('invalid data', 'mits');
        }
        if (!$return_data['error']) {

            $to = "hello@mits.pl";
            $subject = 'Wiadomość z formularza mits.pl';
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Formularz mits.pl <formularz@mits.pl>', 'Reply-To: <' . $to . '>');
            $message = "PAGE: " . $data['page_title'] . "</br>" . "NAME: " . $data['name'] . "</br>" . "EMAIL: " . $data['email_or_number'] . "</br>" . "MESSAGE: " . $data['message'];
            if (!function_exists('debug_wpmail')) :
                function debug_wpmail($result = false)
                {
                    if ($result)
                        return;
                    global $ts_mail_errors, $phpmailer;
                    if (!isset($ts_mail_errors))
                        $ts_mail_errors = array();
                    if (isset($phpmailer))
                        $ts_mail_errors[] = $phpmailer->ErrorInfo;
                    print_r('<pre>');
                    print_r($ts_mail_errors);
                    print_r('</pre>');
                }
            endif;

            $send_mail = wp_mail($to, $subject, $message, $headers);
            if ($send_mail) {
                wp_send_json_success(__('Your message has been sent.', 'mits'));
            } else {
                debug_wpmail($send_mail);
                wp_send_json_error(__('Your message has been sent.', 'mits'));
            }
        }
        wp_send_json_error($return_data);
    } else {
        wp_send_json_error(__('Your message has been sent.', 'mits'));
    }
}
