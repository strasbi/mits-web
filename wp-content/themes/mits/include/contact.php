<?php

add_action('wp_ajax_contact_form_page', 'contact_form_page'); //
add_action('wp_ajax_nopriv_contact_form_page', 'contact_form_page');

function contact_form_page()
{
    if(!verify_captcha($_POST['g-recaptcha-response'])) {
        wp_send_json_error(__("You are a Robot!!", 'mits'));
    }
    $data = esc_sql(maybe_unserialize($_POST));

    $return_data = [];
    $return_error = [];
    if (!$data) {
        wp_send_json_error("cant read post data");
    } else {
        foreach ($data as $key => $single_field) {
            if ($key !== 'cv' && ($single_field === NULL || $single_field === '')) {
                $return_error[$key] = __('This field is required', 'mits');

            } else {
                $return_data[$key] = $single_field;
            }
        }
        if (!preg_match("/@.+\./", $data['email'])) {
            $return_error['email'] = __('Invalid email address', 'mits');
        }
        if ($return_error) {
            wp_send_json_error($return_error);
        } else {

            $subject = "[Contact form MITS] " . $return_data['title'];
            $to = "hello@mits.pl";
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Formularz kontaktowy mits <formularz@mits.pl>', 'Reply-To: <' . $to . '>');
            $body = '';

            foreach ($return_data as $key => $value) {
                $body .= $key . ': ' . $value . '<br/>';
            }

            $send_mail = wp_mail($to, $subject, $body, $headers);

            if ($send_mail) {
                wp_send_json_success(__("Your message has been sent.", 'mits'));
            } else {
                //   debug_wpmail($send_mail);
                wp_send_json_error(__("Couldn't send this message", 'mits'));
            }
        }
    }
}
